var ResponseType = {
    SIGNIN: 1,
    MOVE: 2,
    ATTACK: 3,
    DEATH: 4,
    ADDPLAYER: 5,
    REMOVEPLAYER: 6,
    UPDATEPLAYER: 7,
    ADDOBJECT: 8,
    REMOVEOBJECT: 9,
    MESSAGE: 10
};

if (typeof module !== 'undefined') {
    module.exports = ResponseType;
}

var MapObjectType = {
    CACTUS: 1,
    HUT: 2,
    TREE: 3,
    HEALTH: 4,
    MONASTERY: 5,
    SEVENELEVEN: 6,
    TEMPLE: 7,
    FACTORY: 8,
    HOUSE: 9,
    BURNTTREE: 10,
    BLUEHOUSE: 11,
    BLUEHOUSE2: 12,
    TREEPLAIN: 13,
    TREEAPPLES: 14,
    TREEBLOOMY: 15
};

if (typeof module !== 'undefined') {
    module.exports = MapObjectType;
}

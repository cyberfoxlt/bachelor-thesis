if (typeof module !== 'undefined') {
    var Class       = require('../Class.js'),
    ResponseType    = require('../ResponseType'),
    MessageAbstract = require('../MessageAbstract.js');
}

var MessageResponse = Class.extend(MessageAbstract);

MessageResponse.prototype.init = function(name, text) {
    this.setCode(ResponseType.MESSAGE);
    this.name = name;
    this.text = text;
};

if (typeof module !== 'undefined') {
    module.exports = MessageResponse;
}

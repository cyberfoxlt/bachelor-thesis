if (typeof module !== 'undefined') {
    var Class           = require('../Class.js'),
        ResponseType    = require('../ResponseType'),
        MessageAbstract = require('../MessageAbstract.js');
}

var AttackResponse = Class.extend(MessageAbstract);

AttackResponse.prototype.init = function(attacker_id, victim_id, damage) {
    this.setCode(ResponseType.ATTACK);
    this.attacker_id = attacker_id;
    this.victim_id   = victim_id;
    this.damage      = damage;
};

if (typeof module !== 'undefined') {
    module.exports = AttackResponse;
}

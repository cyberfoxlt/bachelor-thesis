if (typeof module !== 'undefined') {
    var Class = require('../Class.js'),
        ResponseType = require('../ResponseType'),
        MessageAbstract = require('../MessageAbstract.js');
}

var SignInResponse = Class.extend(MessageAbstract);

SignInResponse.prototype.init = function(player_id, x, y, health, type, weapon) {
    this.setCode(ResponseType.SIGNIN);
    this.player_id = player_id;
    this.x = x;
    this.y = y;
    this.health = health;
    this.type = type;
    this.weapon = weapon;
    // TODO add player's name
};

if (typeof module !== 'undefined') {
    module.exports = SignInResponse;
}

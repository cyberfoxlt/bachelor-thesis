if (typeof module !== 'undefined') {
    var Class = require('../Class.js'),
        ResponseType = require('../ResponseType'),
        MessageAbstract = require('../MessageAbstract.js'),
        Weapons = require('../../server/lib/Weapons');
}

var AddPlayerResponse = Class.extend(MessageAbstract);

AddPlayerResponse.prototype.init = function(player_id, name, type, x, y, health, weapon) {
    this.setCode(ResponseType.ADDPLAYER);

    this.player_id = player_id;
    this.name    = name;
    this.type    = type;
    this.x       = x;
    this.y       = y;
    this.health  = health;
    this.weapon  = weapon;
};

// wtf? it is common class!
AddPlayerResponse.build = function(player) {
    return new AddPlayerResponse(
        player.player_id,
        player.name,
        player.type,
        player.location.x,
        player.location.y,
        player.health,
        Weapons.getIdByInstance(player.weapon)
    );
};

if (typeof module !== 'undefined') {
    module.exports = AddPlayerResponse;
}

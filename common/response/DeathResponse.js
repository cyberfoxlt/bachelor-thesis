if (typeof module !== 'undefined') {
    var Class       = require('../Class.js'),
    ResponseType    = require('../ResponseType'),
    MessageAbstract = require('../MessageAbstract.js');
}

var DeathResponse = Class.extend(MessageAbstract);

DeathResponse.prototype.init = function(player_id) {
    this.setCode(ResponseType.DEATH);
    this.player_id = player_id || 0;
};

if (typeof module !== 'undefined') {
    module.exports = DeathResponse;
}

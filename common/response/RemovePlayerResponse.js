if (typeof module !== 'undefined') {
    var Class           = require('../Class.js'),
        ResponseType    = require('../ResponseType'),
        MessageAbstract = require('../MessageAbstract.js');
}

var RemovePlayerResponse = Class.extend(MessageAbstract);

RemovePlayerResponse.prototype.init = function(player_id) {
    this.setCode(ResponseType.REMOVEPLAYER);
    this.player_id = player_id;
};

if (typeof module !== 'undefined') {
    module.exports = RemovePlayerResponse;
}

if (typeof module !== 'undefined') {
    var Class = require('../Class.js'),
            ResponseType = require('../ResponseType'),
            MessageAbstract = require('../MessageAbstract.js');
}

var RemoveObjectResponse = Class.extend(MessageAbstract);

RemoveObjectResponse.prototype.init = function(object_id) {
    this.setCode(ResponseType.REMOVEOBJECT);
    this.object_id = object_id;
};

if (typeof module !== 'undefined') {
    module.exports = RemoveObjectResponse;
}

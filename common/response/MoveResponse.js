if (typeof module !== 'undefined') {
    var Class = require('../Class.js'),
            ResponseType = require('../ResponseType'),
            MessageAbstract = require('../MessageAbstract.js');
}

var MoveResponse = Class.extend(MessageAbstract);
var p = MoveResponse.prototype;

p.init = function(player_id, x, y) {
    this.setCode(ResponseType.MOVE);
    this.player_id = player_id;
    this.x = x;
    this.y = y;
};

if (typeof module !== 'undefined') {
    module.exports = MoveResponse;
}



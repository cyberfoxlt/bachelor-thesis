if (typeof module !== 'undefined') {
    var Class = require('../Class.js'),
        ResponseType = require('../ResponseType'),
        MessageAbstract = require('../MessageAbstract.js');
}

var AddObjectResponse = Class.extend(MessageAbstract);

AddObjectResponse.prototype.init = function(object_id, type, x, y) {
    this.setCode(ResponseType.ADDOBJECT);
    this.object_id = object_id;
    this.type = type;
    this.x = x;
    this.y = y;
};

if (typeof module !== 'undefined') {
    module.exports = AddObjectResponse;
}

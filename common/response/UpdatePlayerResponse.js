if (typeof module !== 'undefined') {
    var Class = require('../Class.js'),
        ResponseType = require('../ResponseType'),
        MessageAbstract = require('../MessageAbstract.js');
}

var UpdatePlayerResponse = Class.extend(MessageAbstract);

UpdatePlayerResponse.prototype.init = function(player_id, attribute, value) {
    this.setCode(ResponseType.UPDATEPLAYER);

    this.player_id = player_id;
    this.attribute = attribute;
    this.value     = value;
};

if (typeof module !== 'undefined') {
    module.exports = UpdatePlayerResponse;
}

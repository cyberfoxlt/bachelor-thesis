if (typeof module !== 'undefined') {
    var Class          = require('./Class.js'),
        ParseException = require('./exception/ParseException');
}

var MessageAbstract = Class.create();
var p = MessageAbstract.prototype;

p.init = function() {
    this.code = null;
};

p.serialize = function() {
    var data = [this.getCode()];

    for(key in this) {
        if (typeof this[key] !== 'function' && key.substring(0,1) !== '_' && key != 'parent') {
            if (this[key] === null) {
                throw new RuntimeException('Unable to serialize "' + key + '".');
            }
            data.push(this[key]);
        }
    };

    return JSON.stringify(data);
};

p.getCode = function() {
    return this.code;
};

p.setCode = function(code) {
    Object.defineProperty(this, 'code', {
        value: code,
        writable: false
    });
};

MessageAbstract.unserialize = function(data) {
    var data = JSON.parse(data);

    if (data === false) {
        throw new ParseException('Unable to parse.');
    }

    var i = 1; // 0 - Command code

    /**
     * Prevents from calling init twice.
     */
    var init = this.prototype.init;
    this.prototype.init = function(){};
    var object = new this();
    this.prototype.init = init;

    /**
     * Fetches init method parameters.
     */
    var accepts = object.init.toString().slice(object.init.toString().indexOf('(')+1, object.init.toString().indexOf(')')).match(/([^\s,]+)/g);

    /**
     * If provided parameters count does not equal method parameter count.
     */
    if (accepts.length !== data.length-1) {
        throw new ParseException('Invalid parameter count.');
    }

    /**
     * Initializing object.
     */
    object.init.apply(object, data.slice(1));

    return object;
};

if (typeof module !== 'undefined') {
    module.exports = MessageAbstract;
}

if (typeof module !== 'undefined') {
    var MapObjectType = require('../MapObjectType');
}

var Occupations = {};

Occupations[MapObjectType.TREE] = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1],
    [0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0]
];

Occupations[MapObjectType.CACTUS] = [
    [0, 0, 1],
    [0, 0, 1]
];

Occupations[MapObjectType.HEALTH] = [
    [0]
];

// 3x4
Occupations[MapObjectType.HUT] = [
    [0, 1, 1, 1],
    [0, 1, 1, 1],
    [0, 1, 1, 1]
];

// 7x5
Occupations[MapObjectType.MONASTERY] = [
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1]
];

// 4x3
Occupations[MapObjectType.SEVENELEVEN] = [
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]
];

// 6x4
Occupations[MapObjectType.FACTORY] = [
    [0, 1, 1, 0],
    [0, 1, 1, 1],
    [0, 1, 1, 1],
    [0, 1, 1, 1],
    [0, 1, 1, 1],
    [0, 1, 1, 0]
];

// 6x4
Occupations[MapObjectType.TEMPLE] = [
    [0, 1, 1, 0],
    [0, 1, 1, 0],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [0, 1, 1, 0],
    [0, 1, 1, 0]
];

// 3x2
Occupations[MapObjectType.HOUSE] = [
    [1, 1],
    [1, 1],
    [1, 1]
];

// 3x4
Occupations[MapObjectType.BURNTTREE] = [
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1]
];

// 7x9
Occupations[MapObjectType.BLUEHOUSE] = [
    [0, 0, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1, 1, 1]
];

// 7x8
Occupations[MapObjectType.BLUEHOUSE2] = [
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1]
];

// 3x3
Occupations[MapObjectType.TREEPLAIN] = [
    [0, 0, 0],
    [0, 0, 1],
    [0, 0, 0]
];

// 3x3
Occupations[MapObjectType.TREEAPPLES] = [
    [0, 0, 0],
    [0, 0, 1],
    [0, 0, 0]
];

// 3x3
Occupations[MapObjectType.TREEBLOOMY] = [
    [0, 0, 0],
    [0, 0, 1],
    [0, 0, 0]
];

if (typeof module !== 'undefined') {
    module.exports = Occupations;
}
if (typeof module !== 'undefined') {
    var Class = require('./Class'),
        IllegalArgumentException = require('./exception/IllegalArgumentException');
}

var Location = Class.create();
var p = Location.prototype;

p.init = function(x, y) {
    if (typeof x === 'undefined' || typeof y === 'undefined') {
        throw new IllegalArgumentException();
    }

    this.x = x;
    this.y = y;
};

p.copy = function() {
    return new Location(this.x, this.y);
};

p.equals = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException();
    }

    if (this.x == location.x && this.y == location.y) {
        return true;
    }
    return false;
};

if (typeof module !== 'undefined') {
    module.exports = Location;
}

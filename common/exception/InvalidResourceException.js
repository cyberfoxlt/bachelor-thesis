var InvalidResourceException = Class.create();
var p = InvalidResourceException.prototype = new Error();

p.init = function(message) {
    this.name = 'InvalidResourceException';
    this.message = message || 'An error occured.';
};
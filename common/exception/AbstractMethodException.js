var AbstractMethodException = Class.create();
var p = AbstractMethodException.prototype = new Error();

p.init = function(message) {
    this.name = 'AbstractMethodException';
    this.message = message || 'An error occured';
};
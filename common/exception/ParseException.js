if (typeof module !== 'undefined') {
    var Class = require('../Class');
}

var ParseException = Class.create();
var p = ParseException.prototype = new Error();

p.init = function(message) {
    this.name = 'ParseException';
    this.message = message || 'An error occured.';
};

if (typeof module !== 'undefined') {
    module.exports = ParseException;
}
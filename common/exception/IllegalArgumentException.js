if (typeof module !== 'undefined') {
    var Class = require('../Class');
}

var IllegalArgumentException = Class.create();
var p = IllegalArgumentException.prototype = new Error();

p.init = function(message) {
    this.name = 'IllegalArgumentException';
    this.message = message || 'An error occured';
};

if (typeof module !== 'undefined') {
    module.exports = IllegalArgumentException;
}
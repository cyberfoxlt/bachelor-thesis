var AttributeType = {
    WEAPON: 1,
    HEALTH: 2,
    TYPE  : 3
};

if (typeof module !== 'undefined') {
    module.exports = AttributeType;
}
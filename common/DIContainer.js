if (typeof module !== 'undefined') {
    var Class = require('./Class');
}

var DIContainer = Class.create();

DIContainer.prototype.init = function() {
    this.dependencies = {};
    this.initialized = {};
};

DIContainer.prototype.get = function(dependency) {
    if (typeof this.initialized[dependency] !== 'undefined') {
        return this.initialized[dependency];
    } else {
        if (typeof this.dependencies[dependency] !== 'undefined') {
            this.initialized[dependency] = this.dependencies[dependency]();
            return this.initialized[dependency];
        } else {
            throw new RuntimeException('Missing dependency: ' + dependency);
        }
    }
};

DIContainer.prototype.register = function(dependency, object) {
    if (typeof object === 'function') {
        this.dependencies[dependency] = object;
    } else {
        this.initialized[dependency] = object;
    }
};

if (typeof module !== 'undefined') {
    module.exports = DIContainer;
}
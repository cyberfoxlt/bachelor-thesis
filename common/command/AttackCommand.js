if (typeof module !== 'undefined') {
    var Class           = require('../Class'),
        CommandType     = require('../CommandType'),
        MessageAbstract = require('../MessageAbstract');
}

var AttackCommand = Class.extend(MessageAbstract);

AttackCommand.prototype.init = function(x, y) {
    this.setCode(CommandType.ATTACK);
    this.x = x;
    this.y = y;
};

if (typeof module !== 'undefined') {
    module.exports = AttackCommand;
}

if (typeof module !== 'undefined') {
    var Class = require('../Class'),
            CommandType = require('../CommandType'),
            MessageAbstract = require('../MessageAbstract');
}

var MessageCommand = Class.extend(MessageAbstract);

MessageCommand.prototype.init = function(text) {
    this.setCode(CommandType.MESSAGE);
    this.text = text;
};

if (typeof module !== 'undefined') {
    module.exports = MessageCommand;
}

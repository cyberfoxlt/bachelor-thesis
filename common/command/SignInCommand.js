if (typeof module !== 'undefined') {
    var Class           = require('../Class'),
        CommandType     = require('../CommandType'),
        MessageAbstract = require('../MessageAbstract');
}

var SignInCommand = Class.extend(MessageAbstract);

SignInCommand.prototype.init = function(username) {
    this.setCode(CommandType.SIGNIN);
    this.username = username;
};

if (typeof module !== 'undefined') {
    module.exports = SignInCommand;
}
if (typeof module !== 'undefined') {
    var Class           = require('../Class'),
        CommandType     = require('../CommandType'),
        MessageAbstract = require('../MessageAbstract');
}

var MoveCommand = Class.extend(MessageAbstract);
var p = MoveCommand.prototype;

p.init = function(x, y) {
    this.setCode(CommandType.MOVE);
    this.x = x;
    this.y = y;
};

if (typeof module !== 'undefined') {
    module.exports = MoveCommand;
}

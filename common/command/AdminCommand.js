if (typeof module !== 'undefined') {
    var Class           = require('../Class'),
        CommandType     = require('../CommandType'),
        MessageAbstract = require('../MessageAbstract');
}

var AdminCommand = Class.extend(MessageAbstract);
var p = AdminCommand.prototype;

p.init = function() {
    this.setCode(CommandType.ADMIN);
};

if (typeof module !== 'undefined') {
    module.exports = AdminCommand;
}

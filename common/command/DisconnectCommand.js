if (typeof module !== 'undefined') {
    var Class           = require('../Class'),
        CommandType     = require('../CommandType'),
        MessageAbstract = require('../MessageAbstract');
}

var DisconnectCommand = Class.extend(MessageAbstract);

DisconnectCommand.prototype.init = function() {
    this.setCode(CommandType.DISCONNECT);
};

if (typeof module !== 'undefined') {
    module.exports = DisconnectCommand;
}

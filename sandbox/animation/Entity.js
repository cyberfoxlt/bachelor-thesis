var Entity = function() {};
var p = Entity.prototype;

p.init = function(location) {
    this.localLocation = location; // {x:0, y:0}
    this.distance      = 64; // 0 - 64
    this.image         = new Image();
    this.image.src     = 'ogre.png';
    this.direction     = 'down';
};

p.draw = function(context) {
    // Image
    context.drawImage(this.image, this.localLocation.x * 64, this.localLocation.y * 64 - this.distance);

    if (this.distance > 0) {
        this.distance-=5;
    }
};
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({port: 1337});

x = 0;
y = 0;
id = 1;
player = null;

setTimeout(function() {
    setInterval(function() {
        var width = 8;//(26 - 1) / 2;
        var height = 16 - 1;

        if (x === width && y !== height) {
            y++;
        } else
        if (x !== width && y === 0) {
            x++;
        } else
        if (x !== 0 && y === height) {
            x--;
        } else
        if (x === 0 && y !== 0) {
            y--;
        }

        player.send('[2, ' + id + ', ' + x + ', ' + y + ']');
    }, 256 * 4);
}, 3000);

wss.on('connection', function(socket) {
    player = socket;
});
// In global scope
var config = {
    // Cell size in pixels
    cell: {
        height: 32,
        width: 32
    },
    // Visible map's size in cells
    bounds: {
        height: 7,
        width: 9
    },
    // All map's size in cells
    map: {
        height: 16,
        width: 26
    },
    // Socket
    socket: {
        ip: 'localhost',
        port: 1337
    }
};

// TODO: does not look good
var visibleLength = {horizontal: ((config.bounds.width - 1) / 2), vertical: ((config.bounds.height - 1) / 2)};
var Map = Class.create();
var p = Map.prototype;

p.init = function(width, height) {
    this.width = width;
    this.height = height;

    // Create layers
    this.groundsLayer = new Layer(width, height);
    this.objectsLayer = new Layer(width, height);
    this.playersLayer = new Layer(width, height);
};

/*
 * In most cases will be false, as ground alway exists
 */
p.isLocationEmpty = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    for (var key in this) {
        if (this[key] instanceof Layer) {
            if (!this[key].isLocationEmpty(location)) {
                return false;
            }
        }
    }
    return true;
};

p.isLocationOccupiable = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    // Do not check location occupiable if location is empty in all layers
    if (this.isLocationEmpty(location)) {
        return true;
    } else {
        for (var key in this) {
            if (this[key] instanceof Layer) {
                if (!this[key].isLocationEmpty(location)) {
                    var object = this[key].getLocationContent(location);
                    if (object.occupation[location.x - object.location.x][location.y - object.location.y]) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
};

/*
 * @return Returns content from passed location on passed layer
 */
p.get = function(layer, location) {
    if (!(layer instanceof Layer)) {
        throw new IllegalArgumentException('Passed argument must be instance of Layer class');
    }
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    return layer.getLocationContent(location);
};

/*
 * Place passted object to the passed layer
 */
p.add = function(layer, object) {
    if (!(layer instanceof Layer)) {
        throw new IllegalArgumentException('Passed argument must be instance of Layer class');
    }
    if (!(object instanceof Entity)) {
        throw new IllegalArgumentException('Passed argument must be instance of Entity class');
    }

    for (var x = 0; x < object.occupation.length; x++) {
        for (var y = 0; y < object.occupation[x].length; y++) {
            layer.occupyLocation(new Location(object.location.x + x, object.location.y + y), object);
        }
    }
};

/*
 * Remove passted object from the passed layer
 */
p.remove = function(layer, object) {
    if (!(layer instanceof Layer)) {
        throw new IllegalArgumentException('Passed argument must be instance of Layer class');
    }
    if (!(object instanceof Entity)) {
        throw new IllegalArgumentException('Passed argument must be instance of Entity class');
    }

    for (var x = 0; x < object.occupation.length; x++) {
        for (var y = 0; y < object.occupation[x].length; y++) {
            layer.clearLocation(new Location(object.location.x + x, object.location.y + y));
        }
    }
};

/*
 * @return Returns null if player does not exist in this location
 */
p.getPlayer = function(location) {
    return this.get(this.playersLayer, location);
};

p.addPlayer = function(player) {
    if (!(player instanceof Player)) {
        throw new IllegalArgumentException('Passed argument must be instance of Player class');
    }

    this.add(this.playersLayer, player);
};

p.removePlayer = function(player) {
    if (!(player instanceof Player)) {
        throw new IllegalArgumentException('Passed argument must be instance of Player class');
    }

    this.remove(this.playersLayer, player);
};

/*
 * @return Returns null if object does not exist in this location
 */
p.getObject = function(location) {
    return this.get(this.objectsLayer, location);
};

p.addObject = function(object) {
    if (!(object instanceof MapObject)) {
        throw new IllegalArgumentException('Passed argument must be instance of MapObject class');
    }

    this.add(this.objectsLayer, object);
};

p.removeObject = function(object) {
    if (!(object instanceof MapObject)) {
        throw new IllegalArgumentException('Passed argument must be instance of Object class');
    }

    this.remove(this.objectsLayer, object);
};

/*
 * @return Returns null if ground does not exist in this location
 */
p.getGround = function(location) {
    return this.get(this.groundsLayer, location);
};

p.addGround = function(ground) {
    if (!(ground instanceof Ground)) {
        throw new IllegalArgumentException('Passed argument must be instance of Ground class');
    }

    this.add(this.groundsLayer, ground);
};

p.removeGround = function(ground) {
    if (!(ground instanceof Ground)) {
        throw new IllegalArgumentException('Passed argument must be instance of Ground class');
    }

    this.remove(this.groundsLayer, ground);
};
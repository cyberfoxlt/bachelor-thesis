var MoveCommand = Class.extend(MessageAbstract);

MoveCommand.prototype.init = function(x, y) {
    this.setCode(CommandType.MOVE);
    this.x = x;
    this.y = y;
};

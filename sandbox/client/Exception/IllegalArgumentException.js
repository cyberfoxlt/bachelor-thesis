var IllegalArgumentException = Class.create();
var p = IllegalArgumentException.prototype = new Error();

p.init = function(message) {
    this.name = 'IllegalArgumentException';
    this.message = message || 'An error occured';
};
var ParseException = Class.create();
var p = ParseException.prototype = new Error();

p.init = function(message) {
    this.name = 'ParseException';
    this.message = message || 'An error occured.';
};
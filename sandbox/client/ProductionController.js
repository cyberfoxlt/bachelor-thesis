var Controller = Class.create();
var p = Controller.prototype;

p.init = function() {
    var self = this;
    this.initCanvas();
    this.map = new Map(config.map.width, config.map.height);
    this.players = [];

    // My player's id
    this.id = 1;
    this.selector = null;
    this.cursor = new Location(0, 0);

    document.addEventListener('mousemove', function(event) {
        self.cursor.x = event.pageX - self.canvas.offsetLeft;
        self.cursor.y = event.pageY - self.canvas.offsetTop;
        if (self.cursor.x < 0) {
            self.cursor.x = 0;
        } else if (self.cursor.x >= self.canvas.width) {
            self.cursor.x = self.canvas.width - 1;
        }
        if (self.cursor.y < 0) {
            self.cursor.y = 0;
        } else if (self.cursor.y >= self.canvas.height) {
            self.cursor.y = self.canvas.height - 1;
        }
    });

    this.canvas.addEventListener('click', function(event) {
        // Location where user want to move
        var location = new Location(
                Math.floor((self.cursor.x + Math.abs(self.translate.x)) / config.cell.width),
                Math.floor((self.cursor.y + Math.abs(self.translate.y)) / config.cell.height));
        // Send command to the server
        self.talker.send(new MoveCommand(location.x, location.y));
        // Set selector
        self.selector = location.copy();
    });

    this.talker = new Talker(config.socket.ip, config.socket.port);

    this.talker.setResponseHandler(ResponseType.MOVE, function(data) {
        var response = MoveResponse.unserialize(data);
        var location = new Location(response.x, response.y);
        var player = self.players[response.player_id];
        self.changePlayerLocation(player, location);
    });

    // Start rendering
    this.render();
};

p.changePlayerLocation = function(player, location) {
    if (!(player instanceof Player)) {
        throw new IllegalArgumentException('Passed argument must be instance of Player class');
    }
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    // Remove player from the map and from player's list
    // if he passed my tracking bounds
    if (player.id !== this.id && !this.players[this.id].isWithinTrackBounds(location)) {
        this.map.removePlayer(player);
        delete this.players[player.id];
        return;
    }

    // Remove player from the map
    this.map.removePlayer(player);
    // Change player's location
    this.players[player.id].setLocation(location.copy());
    // Add player to the map
    this.map.addPlayer(this.players[player.id]);
    // Set move action
    player.setAction(ActionType.MOVE);
    if (player.direction === DirectionType.DOWN || player.direction === DirectionType.UP) {
        player.move.vertical = (player.direction === DirectionType.DOWN ? (-config.cell.height) : (config.cell.height));
    } else {
        player.move.horizontal = (player.direction === DirectionType.RIGHT ? (-config.cell.width) : (config.cell.width));
    }

    // TODO: as we know where we move, we can delete players only from oposite side of moving. Maybe it will be better
    // Delete players that are not visible any more for me
    if (player.id === this.id) {
        for (index in this.players) {
            var p = this.players[index];
            if (p.id !== this.id && !this.players[this.id].isWithinTrackBounds(p.location)) {
                this.map.removePlayer(p);
                delete this.players[p.id];
            }
        }
    }
};

p.render = function() {
    var self = this;
    // Clear canvas
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.save(); // Save current settings
    // Calculate me player's cameras' view
    this.translate = new Location(-this.players[this.id].top.x * config.cell.width,
            -this.players[this.id].top.y * config.cell.height);
    if (((this.players[this.id].location.x >= visibleLength.horizontal && this.players[this.id].direction === DirectionType.LEFT)
            || (this.players[this.id].location.x > visibleLength.horizontal && this.players[this.id].direction === DirectionType.RIGHT))
            && ((this.players[this.id].location.x < (config.map.width - visibleLength.horizontal - 1) && this.players[this.id].direction === DirectionType.LEFT)
            || (this.players[this.id].location.x <= (config.map.width - visibleLength.horizontal - 1) && this.players[this.id].direction === DirectionType.RIGHT))) {
        this.translate.x = this.translate.x - this.players[this.id].move.horizontal;
    }
    if (((this.players[this.id].location.y >= visibleLength.vertical && this.players[this.id].direction === DirectionType.UP)
            || (this.players[this.id].location.y > visibleLength.vertical && this.players[this.id].direction === DirectionType.DOWN))
            && ((this.players[this.id].location.y < (config.map.height - visibleLength.vertical - 1) && this.players[this.id].direction === DirectionType.UP)
            || (this.players[this.id].location.y <= (config.map.height - visibleLength.vertical - 1) && this.players[this.id].direction === DirectionType.DOWN))) {
        this.translate.y = this.translate.y - this.players[this.id].move.vertical;
    }

    // Move our camera
    this.context.translate(this.translate.x, this.translate.y);

    // Calculate mouse location in global map
    var location = new Location(this.cursor.x + Math.abs(this.translate.x), this.cursor.y + Math.abs(this.translate.y));
    // Calculate mouse cell in global map
    var cell = new Location(Math.floor(location.x / config.cell.width), Math.floor(location.y / config.cell.height));

    // Set mouse view
    if (this.map.isLocationOccupiable(cell)) {
        this.context.fillStyle = 'green';
    } else {
        if (this.map.getPlayer(cell) === null) {
            this.context.fillStyle = 'yellow';
        } else {
            this.context.fillStyle = 'red';
        }
    }
    // Draw mouse
    this.context.fillRect(location.x, location.y, config.cell.width, config.cell.height);
    // Draw selector if need
    if (this.selector !== null) {
        this.context.lineWidth = 1;
        this.context.strokeStyle = 'gray';
        this.context.strokeRect(
                self.selector.x * config.cell.width, self.selector.y * config.cell.height,
                config.cell.width, config.cell.height);
        // Remove selector if player reaches target 
        if (this.selector.equals(this.players[this.id].location) && this.players[this.id].action !== ActionType.MOVE) {
            this.selector = null;
        }
    }
    // Draw players on canvas
    for (var x = this.players[this.id].top.x - 1; x <= this.players[this.id].bottom.x + 1; x++) {
        for (var y = this.players[this.id].top.y - 1; y <= this.players[this.id].bottom.y + 1; y++) {
            var location = new Location(x, y);
            var player = this.map.getPlayer(location);
            if (player !== null) {
                player.draw(this.context);
            }
        }
    }
    this.context.restore(); // After all drawings restore settings

    window.requestAnimationFrame(function() {
        self.render();
    });
};

p.initCanvas = function() {
    this.canvas = document.getElementById('map');
    this.context = this.canvas.getContext('2d');
    // Set size for canvas
    this.canvas.width = (config.bounds.width * config.cell.width);
    this.canvas.height = (config.bounds.height * config.cell.height);
    // Move canvas to the center of the screen
    this.canvas.style.top = (document.height - this.canvas.height) / 2 + 'px';
    this.canvas.style.left = (document.width - this.canvas.width) / 2 + 'px';
};
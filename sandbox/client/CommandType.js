var CommandType = {
    SIGNIN: 1,
    MOVE: 2,
    DISCONNECT: 3,
    ATTACK: 4,
    MESSAGE: 5
};
var Player = Class.extend(Entity);
var p = Player.prototype;

p.init = function(id, location) {
    Entity.prototype.init.call(this, id, location, [[1]]);

    this.action = ActionType.IDLE;
    this.direction = DirectionType.DOWN;

    // Visible bounds locations
    this.top = new Location(0, 0);
    this.bottom = new Location(0, 0);
    // Bounds calculation
    this.calculateBounds();

    this.move = {horizontal: 0, vertical: 0};
    this.moveSpeed = 256;
    this.lastMove = null;
};

p.setAction = function(action) {
    // TODO: check if action as parameter is from ActionType

    if (this.action !== action) {
        this.action = action;
    }
};

p.setLocation = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    // Recalculate direction
    if (this.location.x > location.x) {
        this.direction = DirectionType.LEFT;
    } else if (this.location.x < location.x) {
        this.direction = DirectionType.RIGHT;
    } else if (this.location.y > location.y) {
        this.direction = DirectionType.UP;
    } else if (this.location.y < location.y) {
        this.direction = DirectionType.DOWN;
    }

    // Save new location
    this.location = location.copy();
    // As our location has been changed we have to recalculate bounds
    this.calculateBounds();
};

p.calculateBounds = function() {
    // Calculate top and bottom locations
    if (this.location.x - visibleLength.horizontal < 0) {
        this.top.x = 0;
    } else if (this.location.x + visibleLength.horizontal > config.map.width - 1) {
        this.top.x = config.map.width - config.bounds.width;
    } else {
        this.top.x = this.location.x - visibleLength.horizontal;
    }
    this.bottom.x = this.top.x + config.bounds.width - 1;

    if (this.location.y - visibleLength.vertical < 0) {
        this.top.y = 0;
    } else if (this.location.y + visibleLength.vertical > config.map.height - 1) {
        this.top.y = config.map.height - config.bounds.height;
    } else {
        this.top.y = this.location.y - visibleLength.vertical;
    }
    this.bottom.y = this.top.y + config.bounds.height - 1;
};

p.calculateMove = function() {
    // From speed formulation speed = distance / time
    var distance = (new Date().getTime() - this.lastMove) * (config.cell.width / this.moveSpeed);
    if (this.move.horizontal !== 0) {
        var move = Math.min(distance, Math.abs(this.move.horizontal));
        this.move.horizontal += (this.move.horizontal > 0) ? -move : move;
    } else if (this.move.vertical !== 0) {
        var move = Math.min(distance, Math.abs(this.move.vertical));
        this.move.vertical += (this.move.vertical > 0) ? -move : move;
    } else {
        this.action = ActionType.IDLE;
    }
    this.lastMove = new Date().getTime();
};

/*
 * Track bounds is same as visible, but a bit bigger
 * @return Returns true if location is within player's track bounds
 */
p.isWithinTrackBounds = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    if (location.x >= (this.top.x - 1) && location.x <= (this.bottom.x + 1)
            && location.y >= (this.top.y - 1) && location.y <= (this.bottom.y + 1)) {
        return true;
    }
    return false;
};

// TODO: We do not use it
p.isNearBounds = function() {
    return (this.top.x === 0 || this.bottom.x === (config.map.width - 1)
            || this.top.y === 0 || this.bottom.y === (config.map.height - 1));
};

p.draw = function(context) {
    var location = new Location(
            this.location.x * config.cell.width + this.move.horizontal,
            this.location.y * config.cell.height + this.move.vertical);

    // Draw my visible area
//    context.fillStyle = 'rgba(0, 0, 0, .2)';
//    context.fillRect(this.bottom.x * config.cell.width + this.scroll.horizontal, this.bottom.y * config.cell.height + this.scroll.vertical,
//            config.cell.width, config.cell.height);
    // Draw where I am
    context.fillStyle = 'rgba(255, 36, 0, .6)';
    context.fillRect(location.x, location.y, config.cell.width, config.cell.height);
    // Draw text
    var text = 'ID: ' + this.id + ' | DIRECTION: ' + this.direction + ' | ACTION: ' + this.action;
    context.textAlign = 'center';
    context.fillStyle = 'black';
    context.textBaseline = 'middle';
    context.fillText(text, location.x + config.cell.width / 2, location.y + config.cell.height / 2);

    // Move
    this.calculateMove();
};
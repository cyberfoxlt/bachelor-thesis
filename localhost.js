var connect = require('connect');
connect.createServer(
    connect.static(__dirname)
).listen(81);

console.log('Now go to: http://localhost/client/index.html');
var Buster = require('buster'),
    Class  = require('../../common/Class');

Buster.testCase('Class', {
    'Inheritance': function() {
        var Abstract = Class.create();

        Abstract.prototype.id = 64;
        Abstract.prototype.init = function(x) {
            this.x = x || 1;
        };

        var Item = Class.extend(Abstract);
        Item.prototype.callParent = function() {
            this.parent.init.call(this, 128);
        };

        var SubItem = Class.extend(Item);

        SubItem.prototype.callParentTwo = function() {
            this.parent.init.call(this, 512);
        };

        var instance = new SubItem();

        assert(instance instanceof Abstract);
        assert.equals(instance.x, 1);
        assert.equals(instance.parent.id, 64);
        instance.callParent();
        assert.equals(instance.x, 128);
        instance.callParentTwo();
        assert.equals(instance.x, 512);
    }
});
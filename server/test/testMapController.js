var Buster         = require('buster'),
    Server         = require('../lib/Server'),
    MapController  = require('../lib/MapController'),
    Sequence       = require('../lib/Sequence'),
    Location       = require('../../common/Location'),
    EmptyMap       = require('../maps/EmptyMap'),
    DIContainer    = require('../../common/DIContainer');

Buster.testCase('Map Controller', {
    setUp: function() {
        container = new DIContainer();
        container.register('config', require('../config/parameters.json'));
        container.register('map_sequence', new Sequence());
        config = container.get('config');
        map = new MapController();
        map.objectsLayer = new EmptyMap(config.map_size.width, config.map_size.height, container.get('map_sequence')).toArray();
    },
    tearDown: function() {
        delete map;
        delete container;
    },
    'CanMoveTo': function() {
        assert(map.canMoveTo(new Location(12, 12)));
    },
    'PathFinder': function() {
        var path = map.getPath(new Location(0,0), new Location(1,1));
        assert.greater(path.length, 0);
    }
});
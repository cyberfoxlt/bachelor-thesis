var config = module.exports;

config["Tests"] = {
    env: "node",
    rootPath: "../",
    sources: [
        "lib/*"
    ],
    tests: [
        "test*.js"
    ]
};
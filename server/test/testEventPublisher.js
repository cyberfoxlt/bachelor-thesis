var Buster            = require('buster'),
    Bootstrap         = require('./Bootstrap'),
    Class             = require('../../common/Class'),
    DisconnectedEvent = require('../lib/Event/DisconnectedEvent.js'),
    EventType         = require('../lib/EventType'),
    EventPublisher    = require('../lib/EventPublisher');

new Bootstrap();

Buster.testCase('EventPublisher', {
    'Registering event handler successfuly': function() {
        refute.exception(function() {
            container.get('eventPublisher').registerEventHandler(EventType.KILLED, function(){});
        });
    },
    'Registering event handler with invalid handler': function() {
        assert.exception(function() {
            container.get('eventPublisher').registerEventHandler(EventType.DISCONNECTED, 'Test');
        });
    },
    'Publishing event successfuly': function() {
        var HandlerClass = Class.create();
        var handler = new HandlerClass();
        var myEvent = new DisconnectedEvent({});
        
        HandlerClass.prototype.handle = function(event) {    
            assert.equals(event, myEvent);
        };
        
        refute.exception(function() {
            container.get('eventPublisher').registerEventHandler(EventType.KILLED, function() { handler.handle.apply(null, arguments); });
        });
        
        refute.exception(function() {
            container.get('eventPublisher').publish(myEvent);
        });
    }
});
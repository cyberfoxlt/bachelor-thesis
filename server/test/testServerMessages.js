var Buster     = require('buster'),
    Bootstrap  = require('./Bootstrap'),
    UnitOfWork = require('../lib/UnitOfWork'),
    EmptyMap   = require('../maps/EmptyMap'),
    MockSocket = require('../lib/AI/MockSocket'),
    MockDB     = require('./MockDB'),

    ResponseType  = require('../../common/ResponseType'),
    CommandType   = require('../../common/CommandType'),
    MoveResponse  = require('../../common/response/MoveResponse'),
    MoveCommand   = require('../../common/command/MoveCommand'),
    SignInCommand = require('../../common/command/SignInCommand'),
    SignedInEvent = require('../lib/Event/SignedInEvent');

new Bootstrap();

Buster.testCase('Server messages', {
    setUp: function() {
        socket = new MockSocket();
        server = container.get('server');
        server.objectsLayer = new EmptyMap();
    },
    'Serialization': function() {
        var command = new MoveResponse(15, 1, 1);
        var serialized = command.serialize();
        assert.same(serialized, JSON.stringify([ResponseType.MOVE, 15, 1, 1]));
    },
    'Unserialization': function() {
        var command = new MoveResponse(15, 1, 1);
        var serialized = command.serialize();
        var unserialized = MoveResponse.unserialize(serialized);

        assert.equals(command, unserialized);
    },
    'SignInCommand': function() {
        var command = new SignInCommand('John Smith');

        refute.exception(function() {
            var unitOfWork = server.processCommand(command, socket);
            assert.equals(unitOfWork.getEvents().length, 1);
            assert(unitOfWork.getEvents()[0] instanceof SignedInEvent);
            assert(unitOfWork instanceof UnitOfWork);
        });

        assert.greater(server.players.length, 0);
    },
    'MoveCommand': function() {
        refute.exception(function() {
            var unitOfWork = server.processCommand(new SignInCommand('John Smith'), socket);
        });
        
        var command = new MoveCommand(server.players[0].location.x, server.players[0].location.y+1);

        assert(command instanceof MoveCommand); // stating the obvious :)
        refute.exception(function() {
            var unitOfWork = server.processCommand(command, socket);
        });
    } 
});
var Class           = require('../../common/Class'),
    DIContainer     = require('../../common/DIContainer'),
    Server          = require('../lib/Server'),
    Dispatcher      = require('../lib/Dispatcher'),
    EventPublisher  = require('../lib/EventPublisher'),
    AIController    = require('../lib/AI/Controller'),
    MockDB          = require('./MockDB');

module.exports = Bootstrap = Class.create();

Bootstrap.prototype.init = function() {
    /**
     * Global variable registration.
     */
    this.registerGlobals();

    /**
     * Dependency Injection Container in Global scope.
     */
    container = new DIContainer();

    container.register('config',         require('../config/parameters.json'));
    container.register('environment',    'development');
    container.register('database',       new MockDB());
    container.register('eventPublisher', function() { return new EventPublisher(); });
    container.register('logger',         {log:function(){}});

    /**
     * Components
     */
    container.register('ai'        , function() { return new AIController(); });
    container.register('server'    , new Server());
    container.register('dispatcher', new Dispatcher());

    container.get('server').registerCommandHandlers();
};

Bootstrap.prototype.registerGlobals = function() {
    if (typeof timestamp === 'undefined') {
        Object.defineProperty(global, 'timestamp', {
            get         : function() { return new Date().getTime(); },
            configurable: false
        });
    }

    if (typeof random === 'undefined') {
        Object.defineProperty(global, 'random', {
            get         : function() { return function(from, to) { return Math.floor(Math.random()*(to-from+1)+from); }; },
            configurable: false
        });
    }
};
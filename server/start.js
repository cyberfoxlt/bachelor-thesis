var config    = require('./config/parameters.json');
var Bootstrap = require('./lib/Bootstrap');

/**
 * Environment can be set with either:
 * 1. Runtime: "node start <environment>"
 * 2. System environment
 * 3. Config {environment:<environment>}
 */
App = new Bootstrap(config, (process.argv[2] || process.env.NODE_ENV || config.environment || 'development'));
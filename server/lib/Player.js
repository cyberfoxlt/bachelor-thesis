var Class      = require('../../common/Class'),
    Location   = require('../../common/Location'),
    WeaponType = require('../../common/WeaponType'),
    Queue      = require('./Queue'),
    Bounds     = require('./Bounds');

module.exports = Player = Class.create();

var p = Player.prototype;

p.init = function(socket, name, player_id) {
    /**
     * Not writable.
     */
    Object.defineProperties(this, {
        'player_id': {
            value: player_id,
            writable: false
        }
    });

    this.ip          = socket._socket.remoteAddress;
    this.socket      = socket;
    this.health      = 100;
    this.weapon      = null;
    this.type        = 1;
    this.location    = new Location(0, 0);
    this.bounds      = new Bounds(this.location);
    this.target      = null;
    this.name        = name;
    this.path        = [];
    this.actionQueue = new Queue();
    this.nextAction  = timestamp;
    this.speed       = {
        move: 256,
        attack: 1024,
        message: 2048
    },
    this.lastActions = {
        move: 0,
        attack: 0,
        message: 0
    };
};

p.setPath = function(path, keepQueue) {
    if (path.length < 1) {
        this.path = [];
        this.target = null;
    } else {
        this.path   = path;
        this.target = path[path.length-1];
    }

    if (typeof keepQueue === 'undefined' || !keepQueue) {
        this.actionQueue.clear();
    }
};

p.equip = function(weapon) {
    this.weapon = weapon;
};

p.getSocket = function() {
    return this.socket;
};
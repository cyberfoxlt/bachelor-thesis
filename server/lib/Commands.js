var Class             = require('../../common/Class'),
    CommandType       = require('../../common/CommandType'),
    MoveCommand       = require('../../common/command/MoveCommand'),
    SignInCommand     = require('../../common/command/SignInCommand'),
    DisconnectCommand = require('../../common/command/DisconnectCommand'),
    AttackCommand     = require('../../common/command/AttackCommand'),
    MessageCommand     = require('../../common/command/MessageCommand');

module.exports = Commands = {};

Commands[CommandType.MOVE]       = function() { return MoveCommand; };
Commands[CommandType.SIGNIN]     = function() { return SignInCommand; };
Commands[CommandType.DISCONNECT] = function() { return DisconnectCommand; };
Commands[CommandType.ATTACK]     = function() { return AttackCommand; };
Commands[CommandType.MESSAGE]    = function() { return MessageCommand; };

Commands.getIdByInstance = function(instance) {
    for (var index in this) {
        if (instance instanceof this[index]()) {
            return parseInt(index);
        }
    }
};
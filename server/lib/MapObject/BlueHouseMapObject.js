var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = BlueHouseMapObject = Class.extend(MapObject);

var p = BlueHouseMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.BLUEHOUSE,
        location,
        object_id,
        Occupations[MapObjectType.BLUEHOUSE]
    );
};
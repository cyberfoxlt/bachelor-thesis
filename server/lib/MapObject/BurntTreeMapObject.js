var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = BurntTreeMapObject = Class.extend(MapObject);

var p = BurntTreeMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.BURNTTREE,
        location,
        object_id,
        Occupations[MapObjectType.BURNTTREE]
    );
};
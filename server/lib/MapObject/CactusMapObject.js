var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = CactusMapObject = Class.extend(MapObject);

var p = CactusMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.CACTUS,
        location,
        object_id,
        Occupations[MapObjectType.CACTUS]
    );
};
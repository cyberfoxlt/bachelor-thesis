var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = FactoryMapObject = Class.extend(MapObject);

var p = FactoryMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.FACTORY,
        location,
        object_id,
        Occupations[MapObjectType.FACTORY]
    );
};
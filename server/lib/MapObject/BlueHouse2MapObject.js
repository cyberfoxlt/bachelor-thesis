var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = BlueHouse2MapObject = Class.extend(MapObject);

var p = BlueHouse2MapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.BLUEHOUSE2,
        location,
        object_id,
        Occupations[MapObjectType.BLUEHOUSE2]
    );
};
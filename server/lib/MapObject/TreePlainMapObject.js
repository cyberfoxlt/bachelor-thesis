var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = TreePlainMapObject = Class.extend(MapObject);

var p = TreePlainMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.TREEPLAIN,
        location,
        object_id,
        Occupations[MapObjectType.TREEPLAIN]
    );
};
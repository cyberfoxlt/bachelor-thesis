var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = MonasteryMapObject = Class.extend(MapObject);

var p = MonasteryMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.MONASTERY,
        location,
        object_id,
        Occupations[MapObjectType.MONASTERY]
    );
};
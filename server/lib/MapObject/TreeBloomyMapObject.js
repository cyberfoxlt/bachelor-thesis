var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = TreeBloomyMapObject = Class.extend(MapObject);

var p = TreeBloomyMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.TREEBLOOMY,
        location,
        object_id,
        Occupations[MapObjectType.TREEBLOOMY]
    );
};
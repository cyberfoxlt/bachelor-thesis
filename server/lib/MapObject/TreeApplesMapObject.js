var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = TreeApplesMapObject = Class.extend(MapObject);

var p = TreeApplesMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.TREEAPPLES,
        location,
        object_id,
        Occupations[MapObjectType.TREEAPPLES]
    );
};
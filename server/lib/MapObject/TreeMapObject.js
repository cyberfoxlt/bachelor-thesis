var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = TreeMapObject = Class.extend(MapObject);

var p = TreeMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.TREE,
        location,
        object_id,
        Occupations[MapObjectType.TREE]
    );
};
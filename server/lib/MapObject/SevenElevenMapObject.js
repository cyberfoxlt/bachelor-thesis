var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = SevenElevenMapObject = Class.extend(MapObject);

var p = SevenElevenMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.SEVENELEVEN,
        location,
        object_id,
        Occupations[MapObjectType.SEVENELEVEN]
    );
};
var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = HouseMapObject = Class.extend(MapObject);

var p = HouseMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.HOUSE,
        location,
        object_id,
        Occupations[MapObjectType.HOUSE]
    );
};
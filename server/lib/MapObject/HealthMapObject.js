var Class         = require('../../../common/Class.js'),
    MapObject     = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    LootItem      = require('../LootItem'),
    LootType      = require('../LootType'),
    Occupations   = require('../../../common/config/Occupations');

module.exports = HealthMapObject = Class.extend(MapObject);

var p = HealthMapObject.prototype;

p.init = function(object_id, location, regenerateFrequency) {
    this.parent.init.call(
        this,
        MapObjectType.HEALTH,
        location,
        object_id,
        Occupations[MapObjectType.HEALTH]
    );

    this.regenerateFrequency = regenerateFrequency;
    this.isLootable = true;
    this.setLoot();
};

p.setLoot = function() {
    this.lootItems = [
        new LootItem(LootType.HEALTH, random(10, 30))
    ];
};

p.reset        = function() { this.setLoot(); };
p.whenMovedOn  = function(player) { return this.lootItems.splice(0,1); };
p.isDepleted   = function() { return this.lootItems.length === 0; };
p.getLootItems = function() { return this.lootItems; };
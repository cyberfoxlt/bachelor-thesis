var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = HutMapObject = Class.extend(MapObject);

var p = HutMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.HUT,
        location,
        object_id,
        Occupations[MapObjectType.HUT]
    );
};
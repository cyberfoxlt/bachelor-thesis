var Class = require('../../../common/Class.js'),
    MapObject = require('../MapObject.js'),
    MapObjectType = require('../../../common/MapObjectType'),
    Occupations = require('../../../common/config/Occupations');

module.exports = TempleMapObject = Class.extend(MapObject);

var p = TempleMapObject.prototype;

p.init = function(object_id, location) {
    this.parent.init.call(
        this,
        MapObjectType.TEMPLE,
        location,
        object_id,
        Occupations[MapObjectType.TEMPLE]
    );
};
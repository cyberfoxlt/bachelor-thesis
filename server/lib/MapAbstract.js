var Class     = require('../../common/Class'),
    MapObject = require('../lib/MapObject'),
    Location  = require('../../common/Location');

module.exports = MapAbstract = Class.create();
var p = MapAbstract.prototype;

p.init = function(width, height, sequence) {
    this.map      = [];
    this.sequence = sequence;

    // Init map with null values
    for (var x = 0; x < width; x++) {
        this.map[x] = [];
        for (var y = 0; y < height; y++) {
            this.map[x][y] = null;
        }
    }
};

p.addObject = function(object) {
    for (var x in object.occupation) {
        for (var y in object.occupation[x]) {
            this.map[parseInt(object.location.x)+parseInt(x)][parseInt(object.location.y)+parseInt(y)] = object;
        }
    }
};

p.toArray = function() {
    return this.map;
};
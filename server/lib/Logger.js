var Class                = require('../../common/Class'),
    LogEntriesRepository = require('./ORM/LogEntry');

module.exports = Logger = Class.create();

Logger.prototype.init = function() {
    this.buffer = [];
    this.environment = container.get('environment');
    this.repository = new LogEntriesRepository();
};

Logger.prototype.log = function(message, stack, level) {
    if (typeof message === 'undefined') {
        return;
    }
    
    if (typeof level === 'undefined') {
        level = 'debug';
    }
    
    switch(level.toLowerCase()) {
        case 'exception':
        case 'error':
        case 'warning':
        case 'cornercase':
        case 'report':
            this.saveMessage(message, stack, level);
            // Note - this does not break the switch. The message should be outputed either way if in development :)
        default:
            if (this.environment === 'development') {
                console.log(message);
                if (typeof stack !== 'undefined' && stack !== null) {
                    console.log(stack);
                }
            }
    }
};

Logger.prototype.saveMessage = function(message, stack, level) {
    this.repository.create({
        message: message,
        stack: stack,
        level: level
    });
};
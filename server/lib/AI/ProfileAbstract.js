var Class = require('../../common/Class');

module.exports = AIProfileAbstract = Class.create();

var p = AIProfileAbstract.prototype;

p.init = function(player) {
    this.server = container.get('server');
    this.player = player;
};

p.onAttack = function(event) {};
p.onItemAppear = function(event) {};
p.onPlayerAppear = function(event) {};
p.onPlayerMove = function(event) {};
p.onPlayerDeath = function(event) {};
p.onEndPath = function() {};
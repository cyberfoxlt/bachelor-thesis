var Class      = require('../../../../common/Class'),
    Location   = require('../../../../common/Location'),
    UnitOfWork = require('../../UnitOfWork'),
    Player     = require('../../Player'),

    AttackCommand = require('../../../../common/command/AttackCommand');

module.exports = AIProfileGuard = Class.extend(Player);

var p = AIProfileGuard.prototype;

p.init = function(socket, name, player_id, guardLocation, guardDistance, guardPost) {
    Player.prototype.init.apply(this, arguments);
    this.socket.open(this);

    /**
     * Dependencies
     */
    this.config     = container.get('config');
    this.server     = container.get('server');
    this.controller = container.get('ai');
    this.map        = this.server.map;
    
    /**
     * Params
     */
    this.guardMap      = [];
    this.guardLocation = guardLocation;
    this.guardDistance = guardDistance || 2;
    this.guardPost     = guardPost     || null;
};

p.proceed = function(env) {
    var self = this;

    this.env = env;
    this.setGuardMap(this.guardLocation, this.guardDistance);
    this.setGuardPost();
    
    setTimeout(function() { self.getBackToGuard(); }, 3000);
};

p.setGuardMap = function(location, distance) {
    for(var x = location.x-distance; x<=location.x+distance;x++) {
        this.guardMap[x] = [];
        for(var y = location.y-distance; y<=location.y+distance;y++) {
            this.guardMap[x][y]     = 1;
            this.env.guardMap[x][y] = 1;
        }
    }
};

p.setGuardPost = function() {
    if (this.guardPost === null || !(this.guardPost instanceof Location) ||
        !(this.map.canMoveTo(this.guardPost)) || (this.env.guardPosts[this.guardPost.x][this.guardPost.y] !== null)) {
        this.guardPost = this.findGuardPost();
    }

    if (this.guardPost !== null) {
        this.env.guardPosts[this.guardPost.x][this.guardPost.y] = 1;
        
        if (this.actionQueue.length === 0) {
            this.setPath(this.map.getPathAvoidingLocation(this.location, this.guardPost, [this.guardLocation]));
        }
    }
};

p.findGuardPost = function() {
    for(var add = 1; add <=this.guardDistance; add++) {
        for(var y = 0; y<=add; y++) {
            for(var x = 0; x<=add; x++) {
                if (x === 0 && y === 0) continue;

                var possible = [
                    new Location(-x, -y), // top left
                    new Location( x, -y), // top right
                    new Location(-x,  y), // bottom left
                    new Location( x,  y)  // bottom right
                ];

                for(index in possible) {
                    var loc = new Location(this.guardLocation.x+possible[index].x, this.guardLocation.y+possible[index].y);
                    if (this.env.guardPosts[loc.x][loc.y] === null && (this.map.canMoveTo(loc) || this.map.getPlayer(loc) instanceof AIProfileGuard)) {
                        return loc;
                    }
                }
            }
        }
    }

    return null;
};

p.findAnIntruder = function() {
    for(x in this.guardMap) {
        for(y in this.guardMap[x]) {
            if (this.map.getPlayer(new Location(x, y)) !== null && !(this.map.getPlayer(new Location(x, y)) instanceof AIProfileGuard)) {
                return this.map.getPlayer(new Location(x, y));
            }
        }
    }

    return null;
};

p.isGuarded = function(location) {
    return typeof this.guardMap[location.x] !== 'undefined' && typeof this.guardMap[location.x][location.y] !== 'undefined';
};

p.getBackToGuard = function() {
    var self     = this;
    var intruder = this.findAnIntruder();

    if (intruder === null) {
        if (this.guardPost === null) {
            this.guardPost = this.findGuardPost();
        }

        if (this.guardPost !== null && this.nextAction+2000<=timestamp) {
            this.setPath(this.map.getPathAvoidingLocation(this.location, this.guardPost, [this.guardLocation]));
        }
    } else {
        this.controller.processCommand(new AttackCommand(intruder.location.x, intruder.location.y), this.getSocket());
    }

    setTimeout(function() { self.getBackToGuard(); }, 3000);
};

p.onAttack = function(response) {
    var unitOfWork = new UnitOfWork();
    var victim     = this.server.getPlayerById(response.victim_id);
    
    if (response.attacker_id === this.player_id) {
        if (victim instanceof AIProfileGuard) {
            this.actionQueue.clear();
        }
    } else if (victim instanceof AIProfileGuard) {
        var attacker = this.server.getPlayerById(response.attacker_id);

        // defend against attack or if a guard close is under attack.
        if (!(attacker instanceof AIProfileGuard) && Math.abs(this.location.x-victim.location.x) <= 2 && Math.abs(this.location.y-victim.location.y) <=2 ) {
            unitOfWork.addCommand(new AttackCommand(attacker.location.x, attacker.location.y), this.getSocket());
        }
    }

    return unitOfWork;
};

p.onMove = function(response) {
    var unitOfWork = new UnitOfWork();
    var self       = this;

    if (response.player_id === this.player_id) return;

    var mover = this.server.getPlayerById(response.player_id);
    if (mover === null || mover instanceof AIProfileGuard) return;

    if (this.isGuarded(new Location(response.x, response.y))) {
        unitOfWork.addCommand(new AttackCommand(response.x, response.y), this.getSocket());
    }
    
    return unitOfWork;
};

p.setPath = function(path, keepQueue) {
    if (path.length < 1) {
        this.path = [];
        this.target = null;
    } else {
        for(index in path) {
            // if path is calculated and it's through guardLocation, try to recalculate it avoiding it.
            if (path[index].equals(this.guardLocation)) {
                var path = this.map.getPathAvoidingLocation(this.location, path[path.length-1], [this.guardLocation]);
                break;
            }
        }

        this.path   = path;
        this.target = path[path.length-1];
    }

    if (typeof keepQueue === 'undefined' || !keepQueue) {
        this.actionQueue.clear();
    }
};

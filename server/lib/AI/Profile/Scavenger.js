var Class      = require('../../../../common/Class'),
    Location   = require('../../../../common/Location'),
    UnitOfWork = require('../../UnitOfWork'),
    Player     = require('../../Player'),

    AttackCommand = require('../../../../common/command/AttackCommand');

module.exports = AIProfileScavenger = Class.extend(Player);

var p = AIProfileScavenger.prototype;

p.init = function(socket, name, player_id) {
    Player.prototype.init.apply(this, arguments);
    this.socket.open(this);

    /**
     * Dependencies
     */
    this.config     = container.get('config');
    this.server     = container.get('server');
    this.controller = container.get('ai');
    this.map        = this.server.map;

    /**
     * Params
     */
    this.attackPatterns = [
        { health: 10, distance: 6 },
        { health: 25, distance: 4 }
    ];

    this.scavengeDistance = 6;
    this.idleTimeInterval = [0, 10000];
    this.idleTime         = 2000;
    this.moveDistance     = [2,6];
    this.fleeHealth       = 40;
};

p.proceed = function(env) {
    var self = this;
    this.env = env;
    setTimeout(function() { self.scavenge(); }, 1000);
};

p.scavenge = function() {
    var self = this;
    
    if (this.nextAction + this.idleTime <= timestamp) {
        this.setPath(this.findNextPath());
        if (this.health <= this.fleeHealth) {
           this.idleTime = 0;
        } else {
            this.idleTime = random(this.idleTimeInterval[0], this.idleTimeInterval[1]);
        }
    }
    
    setTimeout(function() { self.scavenge(); }, 1000);
};

p.findNextLocation = function() {
    return new Location(
        Math.min(this.config.map_size.width-1, Math.max(0, this.location.x + (random(0,1) === 1 ? -random(this.moveDistance[0], this.moveDistance[1]) : random(this.moveDistance[0], this.moveDistance[1])))),
        Math.min(this.config.map_size.height-1, Math.max(0, this.location.y + (random(0,1) === 1 ? -random(this.moveDistance[0], this.moveDistance[1]) : random(this.moveDistance[0], this.moveDistance[1]))))
    );
};

p.findNextPath = function() {
    for(var i = 1; i <= 10; i++) {
        var location = this.findNextLocation();
        var path     = this.map.getPathAvoidingHeatmap(this.location, location, this.env.guardMap);

        if (path.length>0) {
            return path;
        }
    }

    return [];
};

p.isGuarded = function(location) {
    return typeof this.env.guardMap[location.x] !== 'undefined' && typeof this.env.guardMap[location.x][location.y] !== 'undefined';
};

p.onAttack = function(response) {
    var unitOfWork = new UnitOfWork();

    if (response.victim_id === this.player_id) {
        var attacker = this.server.getPlayerById(response.attacker_id);

        if (this.health <= this.fleeHealth) {
            this.setPath(this.findNextPath());
        } else {
            unitOfWork.addCommand(new AttackCommand(attacker.location.x, attacker.location.y), this.getSocket());
        }
    }

    return unitOfWork;
};

p.onMove = function(response) {
    var unitOfWork = new UnitOfWork();
    var self       = this;

    if (response.player_id === this.player_id) {
        if (this.actionQueue.length === 0) {
            mainLoop:
            for(var x = 0; x < this.config.map_size.width; x++) {
                for(var y = 0; y < this.config.map_size.height; y++) {
                    var player = this.map.getPlayer(new Location(x, y));
                    if (player !== null) {
                        for(index in this.attackPatterns) {
                            var pattern = this.attackPatterns[index];

                            if (player.health <= pattern.health && Math.abs(player.location.x - this.location.x) <= pattern.distance &&
                                Math.abs(player.location.y - this.location.y) <= pattern.distance) {
                                unitOfWork.addCommand(new AttackCommand(player.location.x, player.location.y), this.getSocket());
                                break mainLoop;
                            }
                        }
                    }

                    if (Math.abs(this.location.x - x) <= this.scavengeDistance && Math.abs(this.location.y - y) <= this.scavengeDistance) {
                        if (this.map.containsLoot(new Location(x, y))) {
                            var path = this.map.getPathAvoidingHeatmap(this.location, new Location(x, y), this.env.guardMap);

                            if (path.length > 0) {
                                this.setPath(path);
                                break mainLoop;
                            }
                        }
                    }
                }
            }
        }
    } else{
        var mover = this.server.getPlayerById(response.player_id);
        if (mover === null || mover instanceof AIProfileScavenger) return;
        
    }

    return unitOfWork;
};
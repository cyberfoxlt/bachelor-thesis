var Class     = require('../../../common/Class'),
    WebSocket = require('../../node_modules/ws/lib/WebSocket');


module.exports = MockSocket = Class.create();

var p = MockSocket.prototype;

p.init = function() {
    this.controller = container.get('ai');
    this.server     = container.get('server');
    
    this._socket = {
        remoteAddress: 'ai@localhost'
    };
};

p.readyState = WebSocket.OPEN;

p.send = function(response) {
    this.controller.handleResponse(this, response);
};

p.close = function() {
//    this.readyState = WebSocket.CLOSE;
    this.controller.handleClose(this);
};

p.open = function() {
    this.controller.handleOpen(this);
};
var Class           = require('../../../common/Class'),
    ResponseType    = require('../../../common/ResponseType'),
    MessageAbstract = require('../../../common/MessageAbstract'),
    Server          = require('../Server'),
    MockSocket      = require('./MockSocket'),
    Location        = require('../../../common/Location'),
    EmptyMap        = require('../../maps/EmptyMap'),

    /**
     * AI Profiles
     */
    AIGuard     = require('./Profile/Guard');
    AIScavenger = require('./Profile/Scavenger');

module.exports = AIController = Class.create();

var p = AIController.prototype;

p.init = function() {
    this.config           = container.get('config');
    this.eventPublisher   = container.get('eventPublisher');
    this.server           = container.get('server');
    this.responseHandlers = [];
    this.map              = this.server.map;
    this.players          = [];
    this.env              = {}; // AI environment

    this.initAIEnvironment();
    this.initResponseHandlers();
};

p.initAIEnvironment = function() {
    this.refreshGuardMap();
};

p.addScavengers = function(count) {
    for(var x = 1; x <= count; x++) {
        var scavenger = new AIScavenger(new MockSocket(), 'Scavenger', container.get('map_sequence').next);
        scavenger.equip(new WeaponSword());

        if (this.map.placePlayer(scavenger)) {
            this.server.players.push(scavenger);
            this.eventPublisher.publish(new SignedInEvent(scavenger, []));
            scavenger.proceed(this.env);
        } else {
            scavenger.getSocket().close();
        }
    }
};

p.addGuardUnits = function(locations) {
    for(index in locations) {
        var guard = new AIGuard(new MockSocket(), 'Guard', container.get('map_sequence').next, locations[index]);
        guard.equip(new WeaponSword());

        if (this.map.placePlayer(guard)) {
            this.server.players.push(guard);
            this.eventPublisher.publish(new SignedInEvent(guard, []));
            guard.proceed(this.env);
        } else {
            guard.getSocket().close();
        }
    }
};

p.refreshGuardMap = function() {
    this.env.guardPosts = new EmptyMap(this.config.map_size.width, this.config.map_size.height).toArray();
    this.env.guardMap   = new EmptyMap(this.config.map_size.width, this.config.map_size.height).toArray();

    for(index in this.players) {
        if (this.players[index] instanceof AIGuard) {
            for(x in this.players[index].guardMap) {
                for(y in this.players[index].guardMap[x]) {
                    if (this.players[index].guardMap[x][y] === 1) {
                        this.env.guardMap[x][y] = 1;
                    }
                }
            }

            this.env.guardPosts[this.players[index].x][this.players[index].y] = 1;
        }
    }
};

p.initResponseHandlers = function() {
    var self = this;
    
    this.addResponseHandler(ResponseType.MOVE,   function() { return self.handleMove.apply(self, arguments); });
    this.addResponseHandler(ResponseType.ATTACK, function() { return self.handleAttack.apply(self, arguments); });
};

p.handleAttack = function(socket, response) {
    return this.server.getPlayerBySocket(socket).onAttack(response);
};

p.handleMove = function(socket, response) {
    return this.server.getPlayerBySocket(socket).onMove(response);
};

p.handleClose = function(socket) {
    for(index in this.players) {
        if (this.players[index] === null) {
            this.players.splice(index, 1);
        }
    }

    this.refreshGuardMap();
};

p.handleOpen  = function(socket) {
    console.log('called handleOpen');
    this.players.push(this.server.getPlayerBySocket(socket));
};

p.handleResponse = function(socket, response) {
    if (typeof this.responseHandlers[response.getCode()] !== 'undefined') {
        for(index in this.responseHandlers[response.getCode()]) {
            var unitOfWork = this.responseHandlers[response.getCode()][index](socket, response);

            if (typeof unitOfWork !== 'undefined') {
                for(cmd in unitOfWork.getCommands()) {
                    this.processCommand(unitOfWork.getCommands()[cmd][0], unitOfWork.getCommands()[cmd][1]);
                }
            }
        }
    }
};

p.processCommand = function(command, socket) {
    if (!(command instanceof MessageAbstract) || !(socket instanceof MockSocket)) {
        throw new RuntimeException('Invalid command||socket.');
    }

    this.server.processCommand(command, socket);
};

p.addResponseHandler = function(type, handler) {
    if (typeof this.responseHandlers[type] === 'undefined') {
        this.responseHandlers[type] = [];
    }

    this.responseHandlers[type].push(handler);
};
var Class = require('../../common/Class');

module.exports = QueueItem = Class.create();

QueueItem.prototype.init = function(value, conditions) {
    this.value      = value;
    this.conditions = conditions || null;
};
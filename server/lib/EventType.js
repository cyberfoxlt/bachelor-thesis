module.exports = EventType = {
    SIGNEDIN: 1,
    MOVED: 2,
    ATTACKED: 3,
    MAPFULL: 4,
    DISCONNECTED: 5,
    KILLED: 6,
    MESSAGED: 7,
    PLAYERUPDATED: 8,
    OBJECTREMOVED: 9,
    OBJECTPLACED: 10
};
var Class            = require('../../common/Class'),
    RuntimeException = require('./Exception/RuntimeException');

module.exports = MapObject = Class.create();

var p = MapObject.prototype;

p.init = function(type, location, object_id, occupation) {
    /**
     * Not writable.
     */
    Object.defineProperties(this, {
        'type': {
            value: type,
            writable: false
        },
        'location': {
            value: location,
            writable: false
        },
        'object_id': {
            value: object_id,
            writable: false
        },
        'occupation': {
            value: occupation,
            writable: false
        }
    });

    this.isLootable = false;
};

p.reset        = function() {};
p.isDepleted   = function() { return false; };
p.whenMovedOn  = function(player) { return []; };
p.getLootItems = function() { return []; };
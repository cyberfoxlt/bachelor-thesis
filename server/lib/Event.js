var Class            = require('../../common/Class'),
    RuntimeException = require('./Exception/RuntimeException');

module.exports = Event = Class.create();
var p = Event.prototype;
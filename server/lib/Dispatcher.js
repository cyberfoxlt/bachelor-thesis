var Class      = require('../../common/Class'),
    WebSocket  = require('../node_modules/ws/lib/WebSocket'),
    MockSocket = require('./AI/MockSocket'),
    Weapons    = require('./Weapons'),
    Bounds     = require('./Bounds'),
    EventType  = require('./EventType'),

    /**
     * Commands
     */
    DisconnectCommand = require('../../common/command/DisconnectCommand'),

    /**
     * Responses
     */
    SignInResponse       = require('../../common/response/SignInResponse'),
    AddPlayerResponse    = require('../../common/response/AddPlayerResponse'),
    AddObjectResponse    = require('../../common/response/AddObjectResponse'),
    RemovePlayerResponse = require('../../common/response/RemovePlayerResponse'),
    RemoveObjectResponse = require('../../common/response/RemoveObjectResponse'),
    AttackResponse       = require('../../common/response/AttackResponse'),
    DeathResponse        = require('../../common/response/DeathResponse'),
    MessageResponse      = require('../../common/response/MessageResponse'),
    MoveResponse         = require('../../common/response/MoveResponse'),
    UpdatePlayerResponse = require('../../common/response/UpdatePlayerResponse');

module.exports = CommandDispatcher = Class.create();

var p = CommandDispatcher.prototype;

p.init = function() {
    this.eventPublisher = container.get('eventPublisher');
    this.server         = container.get('server');
    this.registerEventHandlers();
};

p.registerEventHandlers = function() {
    var self = this;

    this.eventPublisher.registerEventHandler(EventType.SIGNEDIN     , function() { return self.onSignedIn.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.MOVED        , function() { return self.onMove.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.DISCONNECTED , function() { return self.onDisconnect.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.ATTACKED     , function() { return self.onAttack.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.KILLED       , function() { return self.onKill.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.MESSAGED     , function() { return self.onChatMessage.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.PLAYERUPDATED, function() { return self.onPlayerUpdate.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.OBJECTREMOVED, function() { return self.onObjectRemoval.apply(self, arguments); });
    this.eventPublisher.registerEventHandler(EventType.OBJECTPLACED , function() { return self.onObjectCreation.apply(self, arguments); });
};

p.onDisconnect = function(event) {
    var player  = event.player;
    if (typeof event.player !== 'undefined') {
        var players = this.server.getPlayersInContext(player.bounds);
        
        for(index in players) {
            this.dispatch(
                players[index].getSocket(),
                new RemovePlayerResponse(player.player_id)
            );
        }
    }
};

p.onMove = function(event) {
    var player         = event.player;
    var previous       = event.previous; // previous location
    var old_players    = this.server.getPlayersInContext(new Bounds(previous)); // players in previous context
    var new_players    = this.server.getPlayersInContext(player.bounds); // players in new context minus players in old context
    var new_players    = new_players.filter(function(i) {return !(old_players.indexOf(i) > -1);});
    var current_sight  = this.server.getPlayersInSight(player.location);
    var previous_sight = this.server.getPlayersInSight(previous);
    var current_sight  = current_sight.filter(function(i) {return !(previous_sight.indexOf(i) > -1);});
    var previous_sight = previous_sight.filter(function(i) {return !(current_sight.indexOf(i) > -1);});
    
    /**
     * Alerting subject about new players
     */
    for(index in new_players) {
        var new_player = new_players[index];
//        console.log('Alerting #' + player.name + ' about ' + new_player.name);
        this.dispatch(
            player.getSocket(),
            AddPlayerResponse.build(new_player)
        );
    }

    /**
     * Alerting subject about movement
     */
    this.dispatch(
        player.getSocket(),
        new MoveResponse(player.player_id, player.location.x, player.location.y)
    );
//    console.log('Alerting ' + player.name + ' about new location.');

    /**
     * Alerting players who previously saw the subject
     */
    for(index in previous_sight) {
        if (previous_sight[index].player_id !== player.player_id) {
//            console.log('Alerting ' + previous_sight[index].name + ' about moving player.');
            this.dispatch(
                previous_sight[index].getSocket(),
                new MoveResponse(player.player_id, player.location.x, player.location.y)
            );
        }
    }

    /**
     * Alerting players who can see the subject
     */
    for(index in current_sight) {
        if (current_sight[index].player_id !== player.player_id) {
//            console.log('Alerting @' + current_sight[index].name + ' about ' + player.name);
            this.dispatch(
                current_sight[index].getSocket(),
                AddPlayerResponse.build(player)
            );
//            console.log('Alerting ' + current_sight[index].name + ' about movement.');
            this.dispatch(
                current_sight[index].getSocket(),
                new MoveResponse(player.player_id, player.location.x, player.location.y)
            );
        }
    }
};

p.onObjectRemoval = function(event) {
    this.dispatchBatch(
        this.server.getPlayers(),
        new RemoveObjectResponse(event.object.object_id)
    );
};

p.onObjectCreation = function(event) {
    this.dispatchBatch(
        this.server.getPlayers(),
        new AddObjectResponse(
            event.object.object_id,
            event.object.type,
            event.object.location.x,
            event.object.location.y
        )
    );
};

p.onPlayerUpdate = function(event) {
    this.dispatchBatch(
        this.server.getPlayersInSight(event.player.location),
        new UpdatePlayerResponse(event.player.player_id, event.attribute, event.value)
    );
};


p.onChatMessage = function(event) {
    this.dispatchBatch(
        this.server.getPlayers(),
        new MessageResponse(event.player.name, event.message)
    );
};

p.onAttack = function(event) {
    this.dispatchBatch(
        this.server.getPlayersInSight(event.victim.location),
        new AttackResponse(event.attacker.player_id, event.victim.player_id, event.damage)
    );
};

p.onKill = function(event) {
    this.dispatchBatch(
        this.server.getPlayersInSight(event.player.location),
        new DeathResponse(event.player.player_id)
    );
};

p.onSignedIn = function(event) {
    var player  = event.player;
    var objects = event.objects;
    var visible_players = this.server.getPlayersInContext(player.bounds);
    var players_sight   = this.server.getPlayersInSight(player.location);

    this.dispatch(
        player.getSocket(),
        new SignInResponse(
            player.player_id,
            player.location.x,
            player.location.y,
            player.health,
            player.type,
            Weapons.getIdByInstance(player.weapon)
        )
    );

    for(index in players_sight) {
        if (player.player_id != players_sight[index].player_id) {
            this.dispatch(
                players_sight[index].getSocket(),
                AddPlayerResponse.build(player)
            );
        }
    }
    
    for(index in visible_players) {
        if (player.player_id != visible_players[index].player_id) {
            this.dispatch(
                player.getSocket(),
                AddPlayerResponse.build(visible_players[index])
            );
        }
    }

    for(index in objects) {
        this.dispatch(
            player.getSocket(),
            new AddObjectResponse(
                objects[index].object_id,
                objects[index].type,
                objects[index].location.x,
                objects[index].location.y
            )
        );
    }
};

p.dispatchBatch = function(players, command) {
    for(index in players) {
        this.dispatch(players[index].getSocket(), command);
    }
};

p.dispatch = function(socket, command) {
    if(!(socket instanceof WebSocket) && !(socket instanceof MockSocket)) {
        throw new RuntimeException('Trying to send a command non WebSocket instance.');
    }

    if (socket.readyState === WebSocket.OPEN) {
        container.get('logger').log('Sending ' + command.serialize());

        if (socket instanceof WebSocket) {
            socket.send(command.serialize());
        } else {
            socket.send(command);
        }
    } else {
        container.get('server').processCommand(new DisconnectCommand(socket), socket, true);
        container.get('logger').log('Socket closed before dispatching', command.serialize(), 'cornercase');
    }
};
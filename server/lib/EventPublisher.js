var Class            = require('../../common/Class'),
    Event            = require('./Event'),
    RuntimeException = require('./Exception/RuntimeException');
    
var EventPublisher = Class.create();

var p = EventPublisher.prototype;

p.init = function() {
    this.eventHandlers = [];
};

p.registerEventHandler = function(eventId, handler) {
    if (typeof eventId !== 'number') {
        throw new RuntimeException('Invalid eventId.');
    }
    
    if (typeof handler !== 'function') {
       throw new RuntimeException('Event handler must be a callable function!'); 
    }
    
    if (typeof this.eventHandlers[eventId] === 'undefined') {
        this.eventHandlers[eventId] = [];
    }
    
    this.eventHandlers[eventId].push(handler);
};

p.publish = function(event) {
    if (!(event instanceof Event)) {
        throw new RuntimeException('Invalid event');
    }
    
    if (typeof this.eventHandlers[event.id] !== 'undefined') {
        for (index in this.eventHandlers[event.id]) {
            this.eventHandlers[event.id][index](event);
        }
    }
};

module.exports = EventPublisher;

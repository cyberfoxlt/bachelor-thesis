var Class = require('../../common/Class');

module.exports = LootItem = Class.create();

LootItem.prototype.init = function(type, value) {
    this.type  = type;
    this.value = value;
};
var Class    = require('../../common/Class'),
    Location = require('../../common/Location');

module.exports = Bounds = Class.create();

/*
 * [ - - - - - ]
 * [ - A - - - ]
 * [ - - * - - ]
 * [ - - - B - ]
 * [ - - - - - ]
 *
 * Bounds = [A(X,Y), B(X,Y)], player location: *
 */
Bounds.prototype.init = function(target) {
    var config = container.get('config');
    var limits = [(config.bounds.width-1)/2, (config.bounds.height-1)/2];
    var top      = new Location(target.x - limits[0], target.y - limits[1]);
    var bottom   = new Location(target.x + limits[0], target.y + limits[1]);

    if (target.x - limits[0]<0) {
        top.x    = 0;
        bottom.x = config.bounds.width - 1;
    }

    if (target.y - limits[1]<0) {
        top.y    = 0;
        bottom.y = config.bounds.height - 1;
    }

    if (target.x + limits[0]>config.map_size.width-1) {
        top.x    = config.map_size.width - config.bounds.width;
        bottom.x = config.map_size.width-1;
    }

    if (target.y + limits[1]>config.map_size.height-1) {
        top.y    = config.map_size.height - config.bounds.height;
        bottom.y = config.map_size.height-1;
    }

    this.points = [top, bottom];
};
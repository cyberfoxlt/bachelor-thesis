var Sequelize = require('../../node_modules/sequelize/index');

module.exports = function() {
    return container.get('database').define('ChatMessage', {
        name: Sequelize.STRING,
        text: Sequelize.STRING
    }, { paranoid: true }); // paranoid - on deletion does not delete the record itself but rather sets deletedAt value.
};
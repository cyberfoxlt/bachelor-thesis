var Class       = require('../../common/Class'),
    WeaponType  = require('../../common/WeaponType'),
    WeaponSword = require('./Weapon/WeaponSword');

module.exports = Weapons = {};

Weapons[WeaponType.SWORD] = function() { return WeaponSword; };

Weapons.getIdByInstance = function(instance) {
    for (var index in this) {
        if (instance instanceof this[index]()) {
            return parseInt(index);
        }
    }
};
var Class           = require('../../common/Class'),
    DIContainer     = require('../../common/DIContainer'),
    Logger          = require('./Logger'),
    Server          = require('./Server'),
    Dispatcher      = require('./Dispatcher'),
    EventPublisher  = require('./EventPublisher'),
    AIController    = require('./AI/Controller'),
    Listener        = require('./Listener'),
    Sequelize       = require('../node_modules/sequelize/index');

module.exports = Bootstrap = Class.create();

Bootstrap.prototype.init = function(config, env) {
    /**
     * Global variable registration.
     */
    this.registerGlobals();
    
    /**
     * Dependency Injection Container in Global scope.
     */
    container = new DIContainer();

    container.register('config'     , config);
    container.register('environment', env);
    container.register('database'   , function() {
        var config = container.get('config').database;
        // mock db
        return {
            define: function() {
                return {
                    create: function() {
                        console.log(arguments);
                        return 1;
                    }
                };
            },
        }
        // return new Sequelize(config.name, config.user, config.pass, {
        //     host: config.host,
        //     port: config.port,
        //     dialect: config.engine,
        //     logging: false,
        //     define: { charset: config.charset, collate: config.collate, syncOnAssociation: true }
        // });
    });
    container.register('eventPublisher', function() { return new EventPublisher(); });
    container.register('logger'        , function() { return new Logger(); });
    
    /**
     * Components
     */
    container.register('ai'        , function() { return new AIController(); });
    container.register('server'    , new Server());
    container.register('dispatcher', new Dispatcher());

    container.get('server').start();
    /**
     * Listener
     */
    var listener = new Listener();

};

Bootstrap.prototype.registerGlobals = function() {
    Object.defineProperty(global, 'timestamp', {
        get         : function() { return new Date().getTime(); },
        configurable: false
    });

    Object.defineProperty(global, 'random', {
        get         : function() { return function(from, to) { return Math.floor(Math.random()*(to-from+1)+from); }; },
        configurable: false
    });
};
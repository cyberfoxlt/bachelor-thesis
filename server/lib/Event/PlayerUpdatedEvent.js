var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = PlayerUpdatedEvent = Class.extend(Event);

PlayerUpdatedEvent.prototype.init = function(player, attribute, value) {
    Object.defineProperty(this, 'id', {value:EventType.PLAYERUPDATED, writable: false});
    this.player    = player;
    this.attribute = attribute;
    this.value     = value;
};
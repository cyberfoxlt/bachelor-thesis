var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = MovedEvent = Class.extend(Event);

MovedEvent.prototype.init = function(player, previous) {
    Object.defineProperty(this, 'id', {value:EventType.MOVED, writable: false});
    this.player   = player;
    this.previous = previous;
};
var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = ObjectPlacedEvent = Class.extend(Event);

ObjectPlacedEvent.prototype.init = function(object) {
    Object.defineProperty(this, 'id', {value:EventType.OBJECTPLACED, writable: false});
    this.object = object;
};
var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = MessagedEvent = Class.extend(Event);

MessagedEvent.prototype.init = function(player, message) {
    Object.defineProperty(this, 'id', {value:EventType.MESSAGED, writable: false});
    this.player  = player;
    this.message = message;
};
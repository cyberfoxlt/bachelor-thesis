var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = DisconnectedEvent = Class.extend(Event);

DisconnectedEvent.prototype.init = function(socket, player) {
    Object.defineProperty(this, 'id', {value:EventType.DISCONNECTED, writable: false});
    this.socket = socket;
    this.player = player;
};
var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = ObjectRemovedEvent = Class.extend(Event);

ObjectRemovedEvent.prototype.init = function(object) {
    Object.defineProperty(this, 'id', {value:EventType.OBJECTREMOVED, writable: false});
    this.object = object;
};
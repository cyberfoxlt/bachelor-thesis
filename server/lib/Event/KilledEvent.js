var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = KilledEvent = Class.extend(Event);

KilledEvent.prototype.init = function(player) {
    Object.defineProperty(this, 'id', {value:EventType.KILLED, writable: false});
    this.player = player;
};
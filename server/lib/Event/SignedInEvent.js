var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = SignedInEvent = Class.extend(Event);

SignedInEvent.prototype.init = function(player, objects) {
    Object.defineProperty(this, 'id', {value:EventType.SIGNEDIN, writable: false});
    this.player  = player;
    this.objects = objects;
};
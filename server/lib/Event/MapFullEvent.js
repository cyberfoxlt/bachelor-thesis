var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = MapFullEvent = Class.extend(Event);

MapFullEvent.prototype.init = function(player) {
    Object.defineProperty(this, 'id', {value:EventType.MAPFULL, writable: false});
    this.player = player;
};
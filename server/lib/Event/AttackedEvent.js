var Class     = require('../../../common/Class.js'),
    EventType = require('../EventType'),
    Event     = require('../Event.js');

module.exports = AttackedEvent = Class.extend(Event);

AttackedEvent.prototype.init = function(attacker, victim, damage) {
    Object.defineProperty(this, 'id', {value:EventType.ATTACKED, writable: false});
    this.attacker = attacker;
    this.victim   = victim;
    this.damage   = damage;
};
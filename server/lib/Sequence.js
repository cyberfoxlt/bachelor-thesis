var Class = require('../../common/Class');

module.exports = Sequence = Class.create();

Sequence.prototype.init = function(start) {
    this.current = start || 1;
    Object.defineProperty(this, 'next', { get: this.next, configurable: false });
};

Sequence.prototype.next = function() {
    return this.current++;
};
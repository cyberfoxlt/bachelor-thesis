var Class            = require('../../common/Class'),
    QueueItem        = require('./QueueItem'),
    RuntimeException = require('./Exception/RuntimeException'),
    Location         = require('../../common/Location');

module.exports = Queue = Class.create();

var p = Queue.prototype;

p.init = function() {
    Object.defineProperty(this, 'length', {
        configurable: false,
        get: function() { return this.queue.length; }
    });
    this.queue = [];
};

p.add = function(item) {
    if (!item instanceof QueueItem) {
        throw new RuntimeException('Queue must consist of queue items.');
    }

    this.queue.push(item);
};

p.clear = function() {
    this.queue = [];
};

p.getNext = function() {
    return this.queue.shift();
};

p.getNextByLocation = function(location) {
    if (!location instanceof Location) {
        throw new RuntimeException('Invalid location.');
    }
    
    for(index in this.queue) {
        var item = this.queue[index];

        if (item.conditions !== null && item.conditions instanceof Location && item.conditions.equals(location)) {
            return this.queue.splice(index, 1).shift();
        }
    }

    return null;
};
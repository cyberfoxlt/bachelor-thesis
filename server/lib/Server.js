var Class         = require('../../common/Class'),
    Command       = require('./Commands'),
    UnitOfWork    = require('./UnitOfWork'),
    Player        = require('./Player'),
    MapController = require('./MapController'),
    Location      = require('../../common/Location'),
    Sequence      = require('./Sequence'),
    WebSocket     = require('../node_modules/ws/lib/WebSocket'),
    MockSocket    = require('./AI/MockSocket'),

    /**
     * Types
     */
    CommandType   = require('../../common/CommandType'),
    AttributeType = require('../../common/AttributeType'),
    LootType      = require('./LootType'),

    /**
     * ORM Repositories
     */
    ChatMessagesRepository = require('./ORM/ChatMessage'),

    /**
     * Exceptions
     */
    RuntimeException = require('./Exception/RuntimeException'),

    /**
     * Commands
     */
    DisconnectCommand = require('../../common/command/DisconnectCommand'),
    SignInCommand     = require('../../common/command/SignInCommand'),

    /**
     * Events
     */
    SignedInEvent      = require('./Event/SignedInEvent'),
    DisconnectedEvent  = require('./Event/DisconnectedEvent'),
    MovedEvent         = require('./Event/MovedEvent'),
    MapFullEvent       = require('./Event/MapFullEvent'),
    AttackedEvent      = require('./Event/AttackedEvent'),
    KilledEvent        = require('./Event/KilledEvent'),
    MessagedEvent      = require('./Event/MessagedEvent'),
    PlayerUpdatedEvent = require('./Event/PlayerUpdatedEvent'),
    ObjectRemovedEvent = require('./Event/ObjectRemovedEvent'),
    ObjectPlacedEvent  = require('./Event/ObjectPlacedEvent'),

    /**
     * Weapons
     */
    WeaponSword = require('./Weapon/WeaponSword'),
    Weapons     = require('./Weapons');

module.exports = Server = Class.create();

var p = Server.prototype;

/**
 * Constructor
 */
p.init = function() {
    container.register('map_sequence', new Sequence());
    
    this.config           = container.get('config');
    this.eventPublisher   = container.get('eventPublisher');
    this.entityIdSequence = container.get('map_sequence');
    this.players          = [];
    this.handlers         = [];
    this.intervals        = {};
    this.iterationSpeed   = 32;
    this.map              = new MapController();
    this.stats            = {
        total: {
            iterations: 0,
            time: 0
        },
        current: [],
        cycles : [],
        lastlog: timestamp
    };
    this.repositories     = {
        chat: new ChatMessagesRepository()
    };
    this.regenerativeObjects = [];
};

/**
 * Called when all dependencies are loaded in the container.
 */
p.start = function() {
    /**
     * Registers regenerative objects from the map before starting the game.
     */
    this.registerRegenerativeObjects();

    /**
     * Registers command handlers to handle messages from the players.
     */
    this.registerCommandHandlers();

    /**
     * Begins internal intervals.
     */
    this.startCycles();

    /**
     * Populates the map with AI players.
     */
    // this.populateAI();
};

/**
 * Initializing command handlers.
 */
p.registerCommandHandlers = function() {
    var self = this;

    this.setCommandHandler(CommandType.SIGNIN,     function() { return self.handleSignIn.apply(self, arguments); });
    this.setCommandHandler(CommandType.MOVE,       function() { return self.handleMove.apply(self, arguments); });
    this.setCommandHandler(CommandType.DISCONNECT, function() { return self.handleDisconnect.apply(self, arguments); });
    this.setCommandHandler(CommandType.ATTACK,     function() { return self.handleAttack.apply(self, arguments); });
    this.setCommandHandler(CommandType.MESSAGE,    function() { return self.handleChatMessage.apply(self, arguments); });
    this.setCommandHandler(CommandType.ADMIN,      function() { return self.handleAdmin.apply(self, arguments); });
};

/**
 * Collects regenerative objects from the map and saves them for later use.
 * If an object is deleted and is regenerative, deletedAt time is set.
 * During checkRegenerative loop cycles the server checks whether the object was deleted,
 * when it was deleted and should it be reset and on the map. If so, calls object reset and
 * places it on the map if object's location is not occupied.
 */
p.registerRegenerativeObjects = function() {
    var objects = this.map.getObjects();
    for(index in objects) {
        var object = objects[index];
        if (object.isLootable) {
            this.regenerativeObjects.push({
                object: object,
                frequency: object.regenerateFrequency || this.config.frequencies.loot_regeneration,
                deletedAt: null
            });
        }
    }
};

/**
 * Loop cycles
 */
p.startCycles = function() {
    var self = this;
    this.intervals.iteration  = setTimeout(function()  { self.iterate(); }, 50);
    this.intervals.loot       = setInterval(function() { self.controlLootable();}, 20000);
    this.intervals.regenerate = setInterval(function() { self.checkRegenerative();}, 2000);
};

/**
 * Populates map with AI players.
 */
p.populateAI = function() {
    container.get('ai').addGuardUnits([
        new Location(13, 12),
        new Location(13, 12),
        new Location(43, 43)
    ]);

    container.get('ai').addScavengers(3);
};

/**
 * Command handlers.
 */
p.handleAdmin = function(command, socket) {
    var player     = this.getPlayerBySocket(socket);
    var unitOfWork = new UnitOfWork();

    player.isAdmin = 1;
    player.bounds = new Bounds(this.config.map_size.width, this.config.map_size.height);

    return unitOfWork;
}

/**
 * Handles MoveCommand. Tries to calculate path to the destination.
 * If successful - sets player path and target to the desired location.
 *
 * @param {MoveCommand} command
 * @param {WebSocket} socket
 * @returns {UnitOfWork}
 */
p.handleMove = function(command, socket) {
    var player     = this.getPlayerBySocket(socket);
    var unitOfWork = new UnitOfWork();
    var path       = this.map.getPath(player.location, new Location(command.x, command.y));

    player.setPath(path);
    
    container.get('logger').log('Player "' + player.name + '" new target: (' + command.x + ':' + command.y + ').');

    return unitOfWork;
};

/**
 * Handles SignInCommand.
 *
 * @param {SignInCommand} command
 * @param {WebSocket} socket
 * @returns {UnitOfWork}
 */
p.handleSignIn = function(command, socket) {
    var unitOfWork = new UnitOfWork();

    var exists = this.getPlayerBySocket(socket);

    if (exists !== null) {
        throw new InvalidMessageException('Player tried to allocate an existing socket. Possibly sending SignIn more than one time from the same socket.');
    }

    if (command.username.length > this.config.limitations.name_length) {
        return unitOfWork;
    }

    var player = new Player(socket, command.username, this.entityIdSequence.next);

    player.equip(new WeaponSword());

    if (this.map.placePlayer(player)) {
        this.players.push(player);
        unitOfWork.addEvent(new SignedInEvent(player, this.map.getObjects()));
        container.get('logger').log('Player "' + player.name + '" signed in.');
    } else {
        unitOfWork.addEvent(new MapFullEvent(player));
    }
    
    container.get('logger').log('Total players: ' + this.players.length);
    
    return unitOfWork;
};

/**
 * Handles disconnect command, removes any ghosts with closed sockets if this kind of stuff happens.
 *
 * @param {DisconnectCommand} command
 * @param {WebSocket} socket
 * @returns {UnitOfWork}
 */
p.handleDisconnect = function(command, socket) {
    var unitOfWork = new UnitOfWork();
    var player     = this.getPlayerBySocket(socket);

    if (player !== null) {
        container.get('logger').log('Player disconnected.');
        for (index in this.players) {
            if (this.players[index].getSocket() == player.socket || this.players[index].socket.readyState !== WebSocket.OPEN) {
                this.removePlayer(this.players[index]);
                unitOfWork.addEvent(new DisconnectedEvent(socket, player));
            }
        }
    }

    socket.close();

    return unitOfWork;
};

/**
 * Handles chat messages.
 *
 * @param {MessageCommand} command
 * @param {WebSocket} socket
 * @returns {UnitOfWork}
 */
p.handleChatMessage = function(command, socket) {
    var unitOfWork = new UnitOfWork();
    var player     = this.getPlayerBySocket(socket);

    // Messages should be several seconds apart for each user to avoid flooding.
    if (player.lastActions.message+player.speed.message > timestamp) {
        container.get('logger').log('Possible flooding from: ' + player.name + '@' + player.ip, null, 'warning');
        return unitOfWork;
    }

    player.lastActions.message = timestamp;

    this.repositories.chat.create({name: player.name, text: command.text});

    unitOfWork.addEvent(new MessagedEvent(player, command.text));

    return unitOfWork;
};

/**
 * Handles attack command.
 *
 * @param {AttackCommand} command
 * @param {WebSocket} socket
 * @returns {UnitOfWork}
 */
p.handleAttack = function(command, socket) {
    var unitOfWork = new UnitOfWork();
    var player     = this.getPlayerBySocket(socket);
    var victim     = this.map.getPlayer(new Location(command.x, command.y));

    if (victim !== null && player !== null && victim.player_id !== player.player_id) {
        if (this.map.isNextTo(player.location, victim.location)) {
            if (player.nextAction > timestamp) {
                player.actionQueue.add(new QueueItem(command, player.location));
            } else {
                container.get('logger').log('Player attacked.');
                var damage = Math.round(player.weapon.attack / 100 * random(50,100));

                victim.health-=damage;
                player.lastActions.attack = timestamp;
                player.nextAction         = timestamp + player.speed.attack;
                
                unitOfWork.addEvent(new AttackedEvent(player, victim, damage));

                if (victim.health <= 0) {
                    unitOfWork.addEvent(new KilledEvent(victim));
                    unitOfWork.addCommand(new DisconnectCommand(), victim.socket);
                } else {
                    player.actionQueue.add(new QueueItem(command, player.location));
                }
            }
        } else {
            var path = this.map.getPathNextTo(player.location, victim.location);
            
            if (path.length > 0) {
                player.setPath(path);
                player.actionQueue.add(new QueueItem(command, player.target));
            }
        }
    }

    return unitOfWork;
};

/**
 * Processes incoming commands from users.
 *
 * @param {CommandAbstract} command
 * @param {WebSocket} socket
 * @param {Boolean} internal
 * @returns {unitOfWork|null}
 */
p.processCommand = function(command, socket, internal) {
    var player = this.getPlayerBySocket(socket);
    if (typeof player === 'undefined' && !(command instanceof SignInCommand) && !(command instanceof DisconnectCommand)) {
        throw new RuntimeException('Player is not signed in.');
    }

    var unitOfWork = this.handleCommand.call(this, command, socket, internal);

    if (unitOfWork instanceof UnitOfWork) {
        for(index in unitOfWork.getEvents()) {
            this.eventPublisher.publish(unitOfWork.getEvents()[index]);
        }

        for(index in unitOfWork.getCommands()) {
            this.processCommand.apply(this, unitOfWork.getCommands()[index], true);
        }
    }

    return unitOfWork;
};

/**
 * Sets command handler.
 *
 * @param {Number} code (CommandType)
 * @param {Callable} handler
 */
p.setCommandHandler = function(code, handler) {
    this.handlers[code] = handler;
};

/**
 * Calls command handler.
 *
 * @param {CommandAbstract} command
 * @param {WebSocket} socket
 * @returns {UnitOfWork|null}
 */
p.handleCommand = function(command, socket) {
    if (typeof this.handlers[command.getCode()] !== 'undefined') {
        return this.handlers[command.getCode()].apply(this, arguments);
    }

    return null;
};


/**
 * Public methods
 */

/**
 * Returns players list.
 *
 * @returns {Array}
 */
p.getPlayers = function() {
    return this.players;
};

/**
 * Fetches players in the given bounds.
 *
 * @param {Bounds} bounds
 * @returns {Array}
 */
p.getPlayersInContext = function(bounds) {
    return this.map.getPlayersInBounds(bounds);
};

/**
 * Fetches players who can see the given location.
 *
 * @param {Location} location
 * @returns {Array}
 */
p.getPlayersInSight = function(location) {
    var players = [];
    
    for(index in this.players) {
        var player = this.players[index];
        if (player.bounds.points[0].x <= location.x && player.bounds.points[1].x >= location.x && player.bounds.points[0].y <= location.y && player.bounds.points[1].y >= location.y) {
            players.push(player);
        }
    }

    return players;
};

/**
 * Fetches player by socket.
 *
 * @param {WebSocket} socket
 * @returns {Player|null}
 */
p.getPlayerBySocket = function(socket) {
    for (var index in this.players) {
        if (this.players[index].getSocket() == socket) {
            return this.players[index];
        }
    }

    return null;
};

/**
 * Fetches players by player_id.
 *
 * @param {Number} player_id
 * @returns {Player|null}
 */
p.getPlayerById = function(player_id) {
    for(var index in this.players) {
        if (this.players[index].player_id === player_id) {
            return this.players[index];
        }
    }

    return null;
};

/**
 * Removes player from the map and from the players list.
 *
 * @param {Player} player
 */
p.removePlayer = function(player) {
    this.map.freeLocation(player.location);
    this.players.splice(this.players.indexOf(player), 1);
};

/**
 * Adds found loot to the player.
 *
 * @param {Player} player
 * @param {LootItem} loot
 */
p.applyPlayerLoot = function(player, loot) {
    switch(loot.type) {
        case LootType.HEALTH:
            player.health+=loot.value;
            this.eventPublisher.publish(new PlayerUpdatedEvent(player, AttributeType.HEALTH, player.health));
            break;
        default:
            container.get('logger').log('Invalid loot type: ' + loot.type + '.', null, 'exception');
    }
};

p.controlLootable = function() {
    var objects = this.map.getLootableObjects();
    var count = {};

    for(key in LootType) count[LootType[key]] = 0;

    for(index in objects) {
        var lootItems = objects[index].getLootItems();
        for(item in lootItems) {
            count[lootItems[item].type]++;
        }
    }
    
    if (count[LootType.HEALTH] < 5) {
        try {
            var newObject = new HealthMapObject(this.entityIdSequence.next, this.map.getFreeLocation());
            this.map.addObject(newObject);
            this.eventPublisher.publish(new ObjectPlacedEvent(newObject));
        } catch(e) {
        }
    }
};

p.checkRegenerative = function() {
    for(index in this.regenerativeObjects) {
        var regenerative = this.regenerativeObjects[index];
        
        if (regenerative.deletedAt !== 0 && regenerative.deletedAt + regenerative.frequency <= timestamp && this.map.isLocationEmpty(regenerative.object.location)) {
            regenerative.deletedAt        = 0;
            regenerative.object.object_id = this.entityIdSequence.next;
            regenerative.object.reset();

            this.map.addObject(regenerative.object);
            this.eventPublisher.publish(new ObjectPlacedEvent(regenerative.object));
        }
    }
};

p.setRegenerativeTimestamp = function(object) {
    for (index in this.regenerativeObjects) {
        var regenerative = this.regenerativeObjects[index];
        if (regenerative.object.object_id === object.object_id) {
            regenerative.deletedAt = timestamp;
        }
    }
};

p.getStats = function(full) {
    var totalCycles = this.stats.cycles.length;
    var stats = {
        playersCount: this.players.length,
        totalIterations: this.stats.total.iterations,
        averageIteration: Math.round((this.stats.cycles.reduce(function(a,b) { return a+b; },0) / totalCycles), 0),
        averageIterationOverall: Math.round(this.stats.total.time / this.stats.total.iterations, 0),
        memoryUsage : process.memoryUsage(),
        uptime      : process.uptime()
//        serverLoad  : {
//            averageLoad: os.loadavg(),
//            memoryTotal: os.totalmem(),
//            memoryFree: os.freemem(),
//            cpuStats: os.cpus()
//        }
    };

    if (full) {
        stats.cycles = this.stats.cycles;
        stats.pastCycle = this.stats.current;
    }

    return stats;
};

p.iterate = function() {
    var start = timestamp;
    for(index in this.players) {
        var player = this.players[index];

        if (player.path.length > 0 && player.nextAction <= timestamp) {
            /**
             * Checks whether the path is still valid or not. If not - tries to recalculate it.
             */
            if (!this.map.pathValid(player.path)) {
                var path = this.map.getPath(player.location, player.target);
                player.setPath(path, true);
            }

            /**
             * If there still is a path, let's move along it.
             */
            if (player.path.length > 0) {
                var previousLocation = new Location(player.location.x, player.location.y);
                if (this.map.movePlayer(player, player.path[0])) {
                    player.setPath(player.path.splice(1), true);
                    player.lastActions.move = timestamp;
                    player.nextAction       = timestamp + player.speed.move;
                    this.eventPublisher.publish(new MovedEvent(player, previousLocation));

                    var atObject = this.map.getObject(player.location);
                    if (atObject !== null && atObject.isLootable) {
                        var loot = atObject.whenMovedOn(player);
                        if (loot.length > 0) {
                            for(index in loot) {
                                this.applyPlayerLoot(player, loot[index]);
                            }

                            if (atObject.isDepleted()) {
                                this.map.removeObject(atObject);
                                this.eventPublisher.publish(new ObjectRemovedEvent(atObject));
                                this.setRegenerativeTimestamp(atObject);
                            }
                        }
                    }
                }
            }
        }

        if (player.actionQueue.length > 0) {
            var action = player.actionQueue.getNextByLocation(player.location);

            if (action !== null) {
                this.processCommand(action.value, player.getSocket(), true);
            }
        }
    }

    this.stats.total.iterations++;
    this.stats.total.time += timestamp - start;
    this.stats.current.push(timestamp - start);

    if (this.stats.current.length > 30) {
        var totalIterations = this.stats.current.length;
        this.stats.cycles.push(Math.round((this.stats.current.reduce(function(a, b) { return a + b; }, 0) / totalIterations),0));

        if (this.stats.lastlog + this.config.frequencies.stats_log <= timestamp) {
            this.stats.lastlog = timestamp;
            container.get('logger').log('Iteration stats.', JSON.stringify(this.getStats(true)), 'report');
            this.stats.cycles = [];
        }
        
        this.stats.current = [];
    }

    if (timestamp - start > 15) {
        container.get('logger').log('Iteration took longer than 15ms.', JSON.stringify(this.stats.current), 'warning');
    }

    var self = this;
    this.intervals.iteration = setTimeout(function() { self.iterate(); }, this.iterationSpeed - (timestamp - start));
};
var WebSocketServer         = require('ws').Server,
    Class                   = require('../../common/Class'),
    Commands                = require('./Commands'),
    UnitOfWork              = require('./UnitOfWork'),
    EventPublisher          = require('./EventPublisher'),
    InvalidMessageException = require('./Exception/InvalidMessageException'),
    DisconnectCommand       = require('../../common/command/DisconnectCommand');

module.exports = Listener = Class.create();

var p = Listener.prototype;

p.init = function() {
    this.server = container.get('server');
    this.config = container.get('config');
    this.listen();
};

p.listen = function() {
    var self = this;
    var wss = new WebSocketServer({port: this.config.port});

    container.get('logger').log('Socket listener initialized.');
    
    // Initialize listeners
    wss.on('connection', function(socket) {
        container.get('logger').log('Player connected');

        socket.on('close', function(code, message) {
            self.server.processCommand(new DisconnectCommand(), this, true);
        });
        
        socket.on('message', function(message) {
            container.get('logger').log('Received: ' + message);
            try {
                self.server.processCommand(self.unserializeCommand(message), socket, false);
            } catch(e) {
                container.get('logger').log(e + '', e.stack + "\n\n ON MESSAGE: " + message, 'exception');
            };
        });
    });
};

p.unserializeCommand = function(message) {
    try {
        var parsed = JSON.parse(message);
    } catch(e) {
        throw new InvalidMessageException('Unable to parse message "' + message + '". ' + e.message + '.');
    }
    
    var commandCode = parsed[0];
    
    if (typeof Commands[commandCode] === 'undefined') {
        throw new InvalidMessageException('Command code not found.');
    }

    return Commands[commandCode]().unserialize(message);
};
var Class            = require('../../common/Class'),
    RuntimeException = require('./Exception/RuntimeException'),
    Location         = require('../../common/Location'),
    AStar            = require('../vendor/AStar'),
    DefaultMap       = require('../maps/DefaultMap'),
    EmptyMap         = require('../maps/EmptyMap'),
    MapObject        = require('./MapObject'),
    Player           = require('./Player'),
    Bounds           = require('./Bounds');

module.exports = MapController = Class.create();
var p = MapController.prototype;

/**
 * Constructor
 */
p.init = function() {
    this.config       = container.get('config');
    this.sequence     = container.get('map_sequence');
    this.objectsLayer = new DefaultMap(this.config.map_size.width, this.config.map_size.height, this.sequence).toArray();
    this.playersLayer = new EmptyMap  (this.config.map_size.width, this.config.map_size.height, this.sequence).toArray();
};

/**
 * Returns the path between two locations.
 *
 * @param {Location} location
 * @param {Location} target
 * @returns {Array}
 */
p.getPath = function(location, target) {
    if (location.equals(target) || !this.locationValid(target)) {
        return [];
    }
    
    var m = [];
    for (var y = 0; y<this.config.map_size.height; y++) {
        m[y] = [];
        for (var x = 0; x<this.config.map_size.width; x++) {
            if (!this.canMoveTo(new Location(x, y))) {
                m[y][x] = 1;
            } else {
                m[y][x] = 0;
            }
        }
    }

    return this.calculatePath(m, location, target);
};

/**
 * Returns the path between two locations avoiding lootable items.
 *
 * @param {Location} location
 * @param {Location} target
 * @returns {Array}
 */
p.getPathAvoidingLoot = function(location, target) {
    if (location.equals(target) || !this.locationValid(target)) {
        return [];
    }

    var m = [];
    for (var y = 0; y<this.config.map_size.height; y++) {
        m[y] = [];
        for (var x = 0; x<this.config.map_size.width; x++) {
            if (!this.canMoveTo(new Location(x, y)) || this.containsLoot(new Location(x, y))) {
                m[y][x] = 1;
            } else {
                m[y][x] = 0;
            }
        }
    }

    return this.calculatePath(m, location, target);
};

/**
 * Returns the path between two locations avoiding given locations.
 *
 * @param {Location} location
 * @param {Location} target
 * @param {Array<Location>} avoid
 * @returns {Array<Location>}
 */
p.getPathAvoidingLocation = function(location, target, avoid) {
    if (location.equals(target) || !this.locationValid(target)) {
        return [];
    }

    var m = [];
    for (var y = 0; y<this.config.map_size.height; y++) {
        m[y] = [];
        for (var x = 0; x<this.config.map_size.width; x++) {
            if (!this.canMoveTo(new Location(x, y))) {
                m[y][x] = 1;
            } else {
                m[y][x] = 0;
            }
        }
    }

    for(index in avoid) {
        m[avoid[index].y][avoid[index].x] = 1;
    }
    
    return this.calculatePath(m, location, target);
};

/**
 * Returns the path between two locations avoiding hitmap.
 *
 * @param {Location} location
 * @param {Location} target
 * @param {Array<Array<Number>>} heatmap
 * @returns {Array<Location>}
 */
p.getPathAvoidingHeatmap = function(location, target, heatmap) {
    if (location.equals(target) || !this.locationValid(target)) {
        return [];
    }

    if (!Array.isArray(heatmap) || !Array.isArray(heatmap[0]) || heatmap.length !== this.config.map_size.width || heatmap[0].length !== this.config.map_size.height) {
        throw new RuntimeException('Invalid hitmap.');
    }

    var m = [];

    for (var y = 0; y<this.config.map_size.height; y++) {
        m[y] = [];
        for (var x = 0; x<this.config.map_size.width; x++) {
            if (heatmap[x][y] === 1 || !this.canMoveTo(new Location(x, y))) {
                m[y][x] = 1;
            } else {
                m[y][x] = 0;
            }
        }
    }

    return this.calculatePath(m, location, target);
};

/**
 * Calculates closest path next to a given location.
 *
 * @param {Location} location
 * @param {Location} target
 * @returns {Array}
 */
p.getPathNextTo = function(location, target) {
    var path = [];
    
    var possibleTargets = [
        new Location(target.x-1, target.y),
        new Location(target.x+1, target.y),
        new Location(target.x,   target.y-1),
        new Location(target.x,   target.y+1)
    ];

    for(i in possibleTargets) {
        var path_arr = this.getPath(location, possibleTargets[i]);

        if (path_arr.length > 0 && (path.length === 0 || path_arr.length < path.length)) {
            path = path_arr; // the closest possible path
        }
    }

    return path;
};

/**
 * Basic path calculation avoiding availabilityMap's 1's.
 *
 * @param {Array} availabilityMap
 * @param {Location} location
 * @param {Location} target
 * @returns {Array}
 */
p.calculatePath = function(availabilityMap, location, target) {
    var path = [];
    
    /**
     * A* path calculation algorithm by Andrea Giammarchi
     * Takes map containing 1's and 0's, current location, the destination and returns array of coordinates.
     */
    var coordinates = AStar(availabilityMap, [location.x, location.y], [target.x, target.y]).splice(1);

    for(index in coordinates) {
        path.push(new Location(coordinates[index][0], coordinates[index][1]));
    }

    return path;
};

/**
 * Finds a random free location on the map.
 * 
 * @returns {Location}
 */
p.getFreeLocation = function() {
    for(var i = 0; i<=100; i++) {
        var x = Math.floor(Math.random() * (this.config.map_size.width-1));
        var y = Math.floor(Math.random() * (this.config.map_size.height-1));
//        var y = 19;
//        var x = 20+i;
        if (this.playersLayer[x][y] === null && this.objectsLayer[x][y] === null) {
            return new Location(x, y);
        }
    }

    throw new RuntimeException('Map is full.');
};

/**
 * Places a player on the map.
 * 
 * @param {Player} player
 * @returns {Boolean}
 */
p.placePlayer = function(player) {
    try {
        var location = this.getFreeLocation();
        this.playersLayer[location.x][location.y] = player;
        player.location = location;
        player.bounds   = new Bounds(location);
        return true;
    } catch(e) {
        container.get('logger').log(e + '', e.stack, 'warning');
        return false;
    }
};

/**
 * Checks whether the player can move to the desired location and moves it if so.
 * 
 * @param {Player} player
 * @param {Location} location
 * @returns {Boolean}
 */
p.movePlayer = function(player, location) {
    if (this.locationValid(location) && this.canMoveTo(location)) {
        this.freeLocation(player.location);
        this.playersLayer[location.x][location.y] = player;
        player.location = location;
        player.bounds   = new Bounds(location);
        return true;
    }
    return false;
};

/**
 * Returns whether player can move to the desired location or not.
 * Takes in consideration objects layer occupation map and players layer.
 *
 * @param {Location} location
 * @returns {Boolean}
 */
p.canMoveTo = function(location) {
    if (this.playersLayer[location.x][location.y] === null && this.objectsLayer[location.x][location.y] === null) {
        return true;
    }

    if (this.playersLayer[location.x][location.y] !== null) {
        return false;
    }

    var object = this.getObject(location);
    var offset = new Location(location.x - object.location.x, location.y - object.location.y);

    return !object.occupation[offset.x][offset.y];
};

/**
 * Returns whether the location contains object which has loot.
 * 
 * @param {Location} location
 * @returns {Boolean}
 */
p.containsLoot = function(location) {
    if (this.getObject(location) !== null) {
        return this.getObject(location).isLootable;
    }

    return false;
};

/**
 * Returns whether the desired location is out of bounds.
 * 
 * @param {Location} location
 * @returns {Boolean}
 */
p.locationValid = function(location) {
    if (location.x >= this.config.map_size.width || location.y >= this.config.map_size.height) {
        return false;
    }

    return true;
};

/**
 * Returns whether the location is completely empty even though you could move at it.
 *
 * @param {Location} location
 * @returns {Boolean}
 */
p.isLocationEmpty = function(location) {
    return this.objectsLayer[location.x][location.y] === null && this.playersLayer[location.x][location.y] === null;
};

/**
 * Returns whether A is next to B.
 *
 * | - + -
 * | + A +
 * | - + -
 *
 * @param {Location} A
 * @param {Location} B
 * @returns {Boolean}
 */
p.isNextTo = function(A, B) {
    if (A.x === B.x && (B.y-1 === A.y || B.y+1 === A.y)) {
        return true;
    }
    
    if (A.y === B.y && (B.x-1 === A.x || B.x+1 === A.x)) {
        return true;
    }

    return false;
};

/**
 * Checks wether the next step of the path is valid.
 *
 * @param {Array} path
 * @returns {Boolean}
 */
p.pathValid = function(path) {
    if (path.length < 1 || !this.canMoveTo(path[0])) {
        return false;
    }

    return true;
};

/**
 * Returns objects layer without object duplication.
 * 
 * @returns {Array}
 */
p.getObjects = function() {
    var objects = [];
    for (var x in this.objectsLayer) {
        for(var y in this.objectsLayer[x]) {
            if (this.objectsLayer[x][y] !== null && this.objectsLayer[x][y].location.equals(new Location(x, y))) {
                objects.push(this.objectsLayer[x][y]);
            }
        }
    }

    return objects;
};

/**
 * Returns players in given bounds.
 *
 * @param {Bounds} bounds
 * @returns {Array}
 */
p.getPlayersInBounds = function(bounds) {
    var players = [];
    var bounds  = bounds.points;
    
    for(var x = bounds[0].x; x <= bounds[1].x; x++) {
        for(var y = bounds[0].y; y <= bounds[1].y; y++) {
            if (this.playersLayer[x][y] !== null && typeof this.playersLayer[x] !== 'undefined' && typeof this.playersLayer[x][y] !== 'undefined') {
                players.push(this.playersLayer[x][y]);
            }
        }
    }
    
    return players;
};

/**
 * Returns player by location.
 *
 * @param {Location} location
 * @returns {Player|null}
 */
p.getPlayer = function(location) {
    return this.playersLayer[location.x][location.y];
};

/**
 * Returns object in given location.
 *
 * @param {Location} location
 * @returns {MapOject}
 */
p.getObject = function(location) {
    return this.objectsLayer[location.x][location.y];
};

// @TODO - needs refactoring. either removeObject, removePlayer or freePlayersLayer, freeObjectsLayer or smth.

/**
 * Returns objects with loot.
 * 
 * @returns {Array}
 */
p.getLootableObjects = function() {
    var objects = [];
    for (var x in this.objectsLayer) {
        for (var y in this.objectsLayer[x]) {
            if (this.objectsLayer[x][y] !== null && this.objectsLayer[x][y].isLootable) {
                objects.push(this.objectsLayer[x][y]);
            }
        }
    }
    
    return objects;
};

/**
 * Adds object to the objects layer.
 *
 * @param {MapObject} object
 */
p.addObject = function(object) {
    for (var x in object.occupation) {
        for (var y in object.occupation[x]) {
            this.objectsLayer[parseInt(object.location.x)+parseInt(x)][parseInt(object.location.y)+parseInt(y)] = object;
        }
    }
};

/**
 * Removes object from the objects layer.
 * 
 * @param {MapObject} object
 */
p.removeObject = function(object) {
    for (var x in object.occupation) {
        for (var y in object.occupation[x]) {
            this.objectsLayer[parseInt(object.location.x)+parseInt(x)][parseInt(object.location.y)+parseInt(y)] = null;
        }
    }
};

/**
 * Free's the desired location from the players layer.
 *
 * @param {Location} location
 */
p.freeLocation = function(location) {
    this.playersLayer[location.x][location.y] = null;
};
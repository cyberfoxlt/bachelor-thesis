var Class = require('../../common/Class');

module.exports = UnitOfWork = Class.create();

var p = UnitOfWork.prototype;

p.init = function() {
    this.events   = [];
    this.commands = [];
};

p.addEvent = function(event) {
    this.events.push(event);
};

p.addCommand = function(command, socket) {
    this.commands.push([command, socket]);
};

p.getEvents = function() {
    return this.events;
};

p.getCommands = function() {
    return this.commands;
};
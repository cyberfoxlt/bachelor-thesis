var Class = require('../../../common/Class');

module.exports = WeaponSword = Class.create();

WeaponSword.prototype.init = function() {
    this.attack  = 10;
    this.defense = 1;
};
var Class = require('../../../common/Class');

module.exports = InvalidMessageException = Class.create();

var p = InvalidMessageException.prototype = new Error();

p.init = function(message) {
    this.name = 'InvalidMessageException';
    this.message = message || 'an error occured.';
};
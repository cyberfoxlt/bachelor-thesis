var Class = require('../../../common/Class');

module.exports = RuntimeException = Class.create();

var p = RuntimeException.prototype = new Error();

p.init = function(message) {
    this.name = 'RuntimeException';
    this.message = message || 'an error occured.';
};
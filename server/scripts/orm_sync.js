var DIContainer            = require('../../common/DIContainer'),
    Sequelize              = require('../node_modules/sequelize/index'),
    ChatMessagesRepository = require('../lib/ORM/ChatMessage'),
    LogEntriesRepository   = require('../lib/ORM/LogEntry');

container = new DIContainer();

container.register('config'     , require('../config/parameters.json'));
container.register('database'   , function() {
    var config = container.get('config').database;
    return new Sequelize(config.name, config.user, config.pass, {
        host: config.host,
        port: config.port,
        dialect: config.engine,
        logging: false,
        define: { charset: config.charset, collate: config.collate, syncOnAssociation: true }
    });
});

/**
 * Syncs tables. Creates if needed.
 */
new LogEntriesRepository().sync();
new ChatMessagesRepository().sync();
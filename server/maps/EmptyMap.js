var Class       = require('../../common/Class'),
    MapAbstract = require('../lib/MapAbstract');

module.exports = EmptyMap = Class.extend(MapAbstract);
var Class           = require('../../common/Class'),
    MapObject       = require('../lib/MapObject'),
    MapAbstract     = require('../lib/MapAbstract'),
    Location        = require('../../common/Location'),
    
    // Map objects
    TreeMapObject        = require('../lib/MapObject/TreeMapObject'),
    CactusMapObject      = require('../lib/MapObject/CactusMapObject'),
    HutMapObject         = require('../lib/MapObject/HutMapObject'),
    HealthMapObject      = require('../lib/MapObject/HealthMapObject'),
    FactoryMapObject     = require('../lib/MapObject/FactoryMapObject'),
    TempleMapObject      = require('../lib/MapObject/TempleMapObject'),
    HouseMapObject       = require('../lib/MapObject/HouseMapObject'),
    SevenElevenMapObject = require('../lib/MapObject/SevenElevenMapObject'),
    MonasteryMapObject   = require('../lib/MapObject/MonasteryMapObject'),
    BlueHouse2MapObject  = require('../lib/MapObject/BlueHouse2MapObject'),
    BlueHouseMapObject   = require('../lib/MapObject/BlueHouseMapObject'),
    BurntTreeMapObject   = require('../lib/MapObject/BurntTreeMapObject'),
    TreeApplesMapObject  = require('../lib/MapObject/TreeApplesMapObject'),
    TreePlainMapObject   = require('../lib/MapObject/TreePlainMapObject'),
    TreeBloomyMapObject  = require('../lib/MapObject/TreeBloomyMapObject');

module.exports = DefaultMap = Class.extend(MapAbstract);

DefaultMap.prototype.init = function() {
    this.parent.init.apply(this, arguments); // Calling parent constructor
    
    // Trees
    // this.addObject(new TreePlainMapObject(this.sequence.next, new Location(3, 3)));
    // this.addObject(new TreeApplesMapObject(this.sequence.next, new Location(3, 9)));
    // this.addObject(new TreeBloomyMapObject(this.sequence.next, new Location(6, 5)));
    this.addObject(new BurntTreeMapObject(this.sequence.next, new Location(3, 3)));
    this.addObject(new TreeMapObject(this.sequence.next, new Location(11, 15)));
    this.addObject(new TreeMapObject(this.sequence.next, new Location(25, 20)));
    this.addObject(new TreeMapObject(this.sequence.next, new Location(30, 9)));
    this.addObject(new TreeMapObject(this.sequence.next, new Location(31, 43)));
    this.addObject(new TreeMapObject(this.sequence.next, new Location(39, 40)));

    // Huts
    this.addObject(new HutMapObject(this.sequence.next, new Location(9, 8)));
    this.addObject(new HutMapObject(this.sequence.next, new Location(15, 25)));
    this.addObject(new HutMapObject(this.sequence.next, new Location(45, 39)));

    // Cactuses
    this.addObject(new CactusMapObject(this.sequence.next, new Location(12, 7)));
    this.addObject(new CactusMapObject(this.sequence.next, new Location(44, 21)));
    this.addObject(new CactusMapObject(this.sequence.next, new Location(47, 33)));

    // // Factory
    // this.addObject(new FactoryMapObject(this.sequence.next, new Location(17, 37)));
    // // Monastery
    // this.addObject(new MonasteryMapObject(this.sequence.next, new Location(4, 37)));
    // // Temple
    // this.addObject(new TempleMapObject(this.sequence.next, new Location(7, 33)));
    // // House
    // this.addObject(new HouseMapObject(this.sequence.next, new Location(13, 42)));
    // // Seven Eleven
    // this.addObject(new SevenElevenMapObject(this.sequence.next, new Location(21, 43)));
    // BLue House
    this.addObject(new BlueHouseMapObject(this.sequence.next, new Location(41, 6)));
    // // Blue House 2
    // this.addObject(new BlueHouse2MapObject(this.sequence.next, new Location(35, 32)));

    // Health
    this.addObject(new HealthMapObject(this.sequence.next, new Location(13, 12)));
    this.addObject(new HealthMapObject(this.sequence.next, new Location(14, 20), 60000));
    this.addObject(new HealthMapObject(this.sequence.next, new Location(28, 25), 60000));
    this.addObject(new HealthMapObject(this.sequence.next, new Location(18, 29), 60000));
    this.addObject(new HealthMapObject(this.sequence.next, new Location(14, 45), 60000));
};
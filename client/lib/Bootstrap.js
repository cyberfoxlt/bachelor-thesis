var Bootstrap = Class.create();
var p = Bootstrap.prototype;

p.init = function() {
    if (!Modernizr.canvas || !Modernizr.audio.ogg || !Modernizr.canvastext) {
        alert('Browser does not support all technologies');
        return 0;
    }
    
    // In global scope
    resourceManager  = new ResourceManager();
    this.loadResources();
};

p.initController = function() {
    controller = new Controller();
};

p.loadResources = function() {
    var self = this;

    // Register all resource that need for the game
    resourceManager.addImage('cursor', 'assets/images/cursor.png');
    resourceManager.addImage('selector', 'assets/images/selector.png');
    resourceManager.addImage('shadow', 'assets/images/shadow.png');

    // MapObjects
    resourceManager.addImage('grass', 'assets/images/grass.png');
    resourceManager.addImage('actor', 'assets/images/actor.png');
    resourceManager.addImage('tree', 'assets/images/tree.png');
    resourceManager.addImage('cactus', 'assets/images/cactus.png');
    resourceManager.addImage('hut', 'assets/images/hut.png');
    resourceManager.addImage('health', 'assets/images/health.png');
    resourceManager.addImage('burnttree', 'assets/images/burnttree.png');
    resourceManager.addImage('bluehouse', 'assets/images/bluehouse.png');
    // Load sounds
    resourceManager.addSound('background', 'assets/sounds/background.ogg');

    resourceManager.addLoadEventListener(function() {
        self.initController();
        resourceManager.getSound('background').play();
    });

    resourceManager.addErrorEventListener(function(data) {
        for (index in data) {
            console.log('Failed load resource: ' + data[index]);
        }
    });
    resourceManager.start();
};
var Layer = Class.create();
var p = Layer.prototype;

p.init = function(width, height) {
    this.width = width;
    this.height = height;
    this.layer = [];

    // Init layers locations with value null
    for (var x = 0; x < this.width; x++) {
        this.layer[x] = [];
        for (var y = 0; y < this.height; y++) {
            this.layer[x][y] = null;
        }
    }
};

p.isLocationEmpty = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    if (this.isLocationWithinBounds(location)) {
        return this.layer[location.x][location.y] === null;
    } else {
        return false;
    }
};

p.clearLocation = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    if (this.isLocationWithinBounds(location)) {
        this.layer[location.x][location.y] = null;
    }
};

p.occupyLocation = function(location, object) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    if (this.isLocationWithinBounds(location)) {
        this.layer[location.x][location.y] = object;
    }
};

p.getLocationContent = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    if (this.isLocationWithinBounds(location)) {
        return this.layer[location.x][location.y];
    } else {
        return null;
    }
};

/*
 * @return Returns true if location is within layer bounds, otherwise return false
 */
p.isLocationWithinBounds = function(location) {
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    if (location.x < 0 || location.x >= this.width
            || location.y < 0 || location.y >= this.height) {
        return false;
    }
    return true;
};
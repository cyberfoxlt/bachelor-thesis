var TreeMapObject = Class.extend(MapObject);
var p = TreeMapObject.prototype;

p.init = function(id, location) {
    MapObject.prototype.init.call(this, id, location, Occupations[MapObjectType.TREE], MapObjectType.TREE);
    this.image = resourceManager.getImage('tree');
};
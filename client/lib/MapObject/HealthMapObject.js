var HealthMapObject = Class.extend(MapObject);
var p = HealthMapObject.prototype;

p.init = function(id, location) {
    MapObject.prototype.init.call(this, id, location, Occupations[MapObjectType.HEALTH], MapObjectType.HEALTH);
    this.image = resourceManager.getImage('health');
};
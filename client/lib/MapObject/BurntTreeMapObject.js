var BurntTreeMapObject = Class.extend(MapObject);
var p = BurntTreeMapObject.prototype;

p.init = function(id, location) {
    MapObject.prototype.init.call(this, id, location, Occupations[MapObjectType.BURNTTREE], MapObjectType.BURNTTREE);
    this.image = resourceManager.getImage('burnttree');
};
var HutMapObject = Class.extend(MapObject);
var p = HutMapObject.prototype;

p.init = function(id, location) {
    MapObject.prototype.init.call(this, id, location, Occupations[MapObjectType.HUT], MapObjectType.HUT);
    this.image = resourceManager.getImage('hut');
};
var CactusMapObject = Class.extend(MapObject);
var p = CactusMapObject.prototype;

p.init = function(id, location) {
    MapObject.prototype.init.call(this, id, location, Occupations[MapObjectType.CACTUS], MapObjectType.CACTUS);
    this.image = resourceManager.getImage('cactus');
};
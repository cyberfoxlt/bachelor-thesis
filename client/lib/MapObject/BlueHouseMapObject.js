var BlueHouseMapObject = Class.extend(MapObject);
var p = BlueHouseMapObject.prototype;

p.init = function(id, location) {
    MapObject.prototype.init.call(this, id, location, Occupations[MapObjectType.BLUEHOUSE], MapObjectType.BLUEHOUSE);
    this.image = resourceManager.getImage('bluehouse');
};
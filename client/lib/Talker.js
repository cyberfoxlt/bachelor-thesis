var Talker = Class.create();
var p = Talker.prototype;

p.init = function(ip, port) {
    this.socket = null;

    this.ip = ip;
    this.port = port;

    // Connection handlers
    this.openHandler = null;
    this.closeHandler = null;
    // Response handlers
    this.handlers = [];

    // Try to create connection
    this.connect();
};

p.connect = function() {
    var self = this;
    this.socket = new WebSocket('ws://' + this.ip + ':' + this.port);

    this.socket.onopen = function() {
        if (self.openHandler !== null) {
            self.openHandler();
        }
    };

    this.socket.onmessage = function(event) {
        self.receiveResponse(event.data);
    };

    this.socket.onclose = function() {
        if (self.closeHandler !== null) {
            self.closeHandler();
        }
    };

    this.socket.onerror = function(error) {
        // TODO: use logger
        console.log('Socket unknown error. Code: ' + error.code);
    };
};

p.send = function(command) {
    if (!(command instanceof MessageAbstract)) {
        throw new IllegalArgumentException('Passed argument must be instance of MessageAbstract class');
    }
    this.socket.send(command.serialize());
};

p.receiveResponse = function(message) {
    var responseCode = JSON.parse(message)[0];
    if (typeof this.handlers[responseCode] === 'undefined') {
        throw new RuntimeException('Cannot find handler');
    }
    this.handlers[responseCode](message);
};

p.setResponseHandler = function(code, handler) {
    this.handlers[code] = handler;
};

p.setOpenHandler = function(handler) {
    this.openHandler = handler;
};

p.setCloseHandler = function(handler) {
    this.closeHandler = handler;
};
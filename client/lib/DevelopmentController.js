var Controller = Class.create();
var p = Controller.prototype;

p.init = function() {
    var self = this;
    this.initCanvas();
    this.map = new Map(config.map.width, config.map.height);
    this.players = [];

    this.talker = new Talker(config.socket.ip, config.socket.port);

    this.talker.setResponseHandler(ResponseType.MOVE, function(data) {
        var response = MoveResponse.unserialize(data);
        var location = new Location(response.x, response.y);
        var player = self.players[response.player_id];
        self.changePlayerLocation(player, location);
    });

    this.talker.send(new SignInCommand('Admin'));
    // Start rendering
    this.render();
};

p.changePlayerLocation = function(player, location) {
    if (!(player instanceof Player)) {
        throw new IllegalArgumentException('Passed argument must be instance of Player class');
    }
    if (!(location instanceof Location)) {
        throw new IllegalArgumentException('Passed argument must be instance of Location class');
    }

    // Remove player from the map
    this.map.removePlayer(player);
    // Change player's location
    this.players[player.id].setLocation(location.copy());
    // Add player to the map
    this.map.addPlayer(this.players[player.id]);
    // Set move action
    player.setAction(ActionType.MOVE);
    if (player.direction === DirectionType.DOWN || player.direction === DirectionType.UP) {
        player.move.vertical = (player.direction === DirectionType.DOWN ? (-config.cell.height) : (config.cell.height));
    } else {
        player.move.horizontal = (player.direction === DirectionType.RIGHT ? (-config.cell.width) : (config.cell.width));
    }
};

p.render = function() {
    var self = this;
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    for (var x = 0; x < config.map.width; x++) {
        for (var y = 0; y < config.map.height; y++) {
            var location = new Location(x, y);
            var player = this.map.getPlayer(location);
            if (player !== null) {
                player.draw(this.context);
            }
        }
    }

    window.requestAnimationFrame(function() {
        self.render();
    });
};

p.initCanvas = function() {
    this.canvas = document.getElementById('map');
    this.context = this.canvas.getContext('2d');
    // Set size for canvas
    this.canvas.width = (config.map.width * config.cell.width);
    this.canvas.height = (config.map.height * config.cell.height);
    // Move canvas to the center of the screen
    this.canvas.style.top = (document.height - this.canvas.height) / 2 + 'px';
    this.canvas.style.left = (document.width - this.canvas.width) / 2 + 'px';
};
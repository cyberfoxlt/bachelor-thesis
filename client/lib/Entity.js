var Entity = Class.create();
var p = Entity.prototype;

p.init = function(id, location, occupation) {
    this.id = id;
    this.location = location;
    this.occupation = occupation;
};

p.draw = function(context) {
    throw new AbstractMethodException('Called Entity\'s an abstract method');
};
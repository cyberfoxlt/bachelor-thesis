var ResourceManager = Class.create();
var p = ResourceManager.prototype;

/**
 * Constructor
 */
p.init = function() {
    this.loadEventListeners = [];
    this.errorEventListeners = [];

    this.errors = [];
    this.loads = [];
    this.all = 0;

    this.resources = {
        images: {},
        sounds: {}
    };
};

p.getImage = function(name) {
    if (typeof this.resources.images[name] === 'undefined') {
        throw new InvalidResourceException('Image "' + name + '" does not exist.');
    }
    return this.resources.images[name].object;
};

p.getSound = function(name) {
    if (typeof this.resources.sounds[name] === 'undefined') {
        throw new InvalidResourceException('Sound "' + name + '" does not exist.');
    }
    return this.resources.sounds[name].object;
};

/**
 * Adds image to resource load queue.
 *
 * @param {String} name
 * @param {String} path
 */
p.addImage = function(name, path) {
    if (typeof this.resources.images[name] === 'undefined') {
        this.resources.images[name] = {
            path: path,
            object: null
        };
        this.all++;
    } else {
        throw new InvalidResourceException('Image "' + name + '" already exists.');
    }
};

/**
 * Adds audio to resource load queue.
 *
 * @param {String} name
 * @param {String} path
 */
p.addSound = function(name, path) {
    if (typeof this.resources.sounds[name] === 'undefined') {
        this.resources.sounds[name] = {
            path: path,
            object: null
        };
        this.all++;
    } else {
        throw new InvalidResourceException('Sound "' + name + '" already exists.');
    }
};

/**
 * Registers load complete event listener.
 *
 * @param {Object} handler
 */
p.addLoadEventListener = function(handler) {
    this.loadEventListeners.push(handler);
};

/**
 * Registers load failed event listener.
 *
 * @param {Object} handler
 */
p.addErrorEventListener = function(handler) {
    this.errorEventListeners.push(handler);
};

/**
 * Called with every resource load. Determines whether the load is complete and calls the listeners.
 */
p.resourceLoaded = function() {
    // If all resources was loaded
    if ((this.loads.length + this.errors.length) == this.all) {
        if (this.errors.length > 0) {
            for (index in this.errorEventListeners) {
                this.errorEventListeners[index](this.errors);
            }
        } else {
            for (index in this.loadEventListeners) {
                this.loadEventListeners[index]();
            }
        }
    }
};

/**
 * Loader.
 */
p.start = function() {
    var self = this;

    for (var type in this.resources) {
        for (var name in this.resources[type]) {
            switch (type) {
                case 'images':
                    // Create image
                    var resource = new Image();
                    resource.src = this.resources[type][name].path;
                    // Add load completion handler
                    resource.addEventListener('load', function(event) {
                        self.loads.push(this.src);
                        self.resourceLoaded();
                    });
                    break;
                case 'sounds':
                    // Create audio
                    var resource = new Audio(this.resources[type][name].path);
                    // Add load completion handlers
                    resource.addEventListener('canplaythrough', function(event) {
                        self.loads.push(this.src);
                        self.resourceLoaded();
                    });
                    break;
                default:
                    throw new InvalidResourceException('Invalid resource type');
            }

            // Save resource
            this.resources[type][name].object = resource;

            // Error handeler
            resource.addEventListener('error', function(event) {
                self.errors.push(this.src);
                self.resourceLoaded();
            });
        }
    }
};
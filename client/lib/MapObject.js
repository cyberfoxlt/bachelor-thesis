var MapObject = Class.extend(Entity);
var p = MapObject.prototype;

p.init = function(id, location, occupation, type) {
    Entity.prototype.init.call(this, id, location, occupation);
    this.type = type;
    this.image = null;
};

p.draw = function(context) {
    context.drawImage(this.image, this.location.x * config.cell.width, this.location.y * config.cell.height);
}